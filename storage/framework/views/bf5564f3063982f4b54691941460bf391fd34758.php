

<?php $__env->startSection('head'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-select.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/selectize.css')); ?>">
<script src="<?php echo e(asset('js/vendor/bootstrap-select.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/vendor/selectize.js')); ?>"></script>
<script src="<?php echo e(asset('js/dropzone.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
<?php echo Breadcrumbs::render('admin.manga.edit', $manga); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-pencil-square-o"></i> <?php echo e(Lang::get('messages.admin.manga.edit.title')); ?>

            </div>
            <!-- /.panel-heading -->
            <?php echo e(Form::open(array('route' => array('admin.manga.update', $manga->id), 'method' => 'PUT'))); ?>

            <div class="box-body">
                <div class="form-group">
                    <?php echo e(Form::label('name', Lang::get('messages.admin.manga.create.manga-name'))); ?>

                    <?php echo e(Form::text('name', $manga->name, array('class' => 'form-control'))); ?>

                    <?php echo $errors->first('name', '<label class="error" for="name">:message</label>'); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('slug', Lang::get('messages.admin.manga.create.manga-slug'))); ?>

                    <?php echo e(Form::text('slug', $manga->slug, array('class' => 'form-control', 'placeholder' => Lang::get('messages.admin.manga.create.slug-placeholder')))); ?>

                    <?php echo $errors->first('slug', '<label class="error" for="slug">:message</label>'); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('otherNames', Lang::get('messages.admin.manga.create.other-names'))); ?>

                    <?php echo e(Form::text('otherNames', $manga->otherNames, array('class' => 'form-control'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('author', Lang::get('messages.admin.manga.create.author'))); ?>

                    <?php echo e(Form::text('author', $authors_id, array('class' => 'authors'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('artist', Lang::get('messages.admin.manga.create.artist'))); ?>

                    <?php echo e(Form::text('artist', $artists_id, array('class' => 'authors'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('releaseDate', Lang::get('messages.admin.manga.create.released'))); ?>

                    <?php echo e(Form::text('releaseDate', $manga->releaseDate, array('class' => 'form-control'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('status', Lang::get('messages.admin.manga.create.status'))); ?>

                    <?php echo e(Form::select('status_id', $status, $manga->status_id, array('class' => 'selectpicker', 'data-width' => '100%'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('categories', Lang::get('messages.admin.manga.create.categories'))); ?>

                    <?php echo e(Form::select('categories[]', $categories, $categories_id, array('class' => 'categories selectpicker', 'multiple', 'title' => Lang::get('messages.admin.manga.create.categories-title'), 'data-selected-text-format' => 'count>7', 'data-width' => '100%'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('tags', Lang::get('messages.admin.manga.create.tags'))); ?>

                    <?php echo e(Form::text('tags', $tags_id, array('class' => 'tags'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('comicType', 'Comic type')); ?>

                    <?php echo e(Form::select('type_id', $comicTypes, $manga->type_id, array('class' => 'status selectpicker', 'data-width' => '100%'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('summary', Lang::get('messages.admin.manga.create.summary'))); ?>

                    <?php echo e(Form::textarea('summary', $manga->summary, array('class' => 'form-control', 'rows' => '5'))); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('caution', Lang::get('messages.admin.manga.detail.caution'))); ?>

                    <label class="radio-inline">
                        <input type="radio" name="caution" value="1" <?php if ($manga->caution == 1): ?>
                               checked="checked"<?php endif ?>/
                               ><?php echo e(Lang::get('messages.admin.users.options.yes')); ?> </label>
                    <label class="radio-inline">
                        <input type="radio" name="caution" value="0" <?php if ($manga->caution == 0): ?>
                               checked="checked"<?php endif ?>
                               /><?php echo e(Lang::get('messages.admin.users.options.no')); ?> </label>
                </div>
            </div>
            <!-- /.panel-body -->
            <div class="box-footer">
                <div class="pull-right">
                    <?php echo e(link_to_route('admin.manga.show', Lang::get('messages.admin.manga.back'), $manga->id, array('class' => 'btn btn-default btn-xs'))); ?>


                    <?php if((Sentinel::check()->id==$manga->user->id && Sentinel::hasAccess('manage_my_manga')) || Sentinel::hasAccess('manga.manga.edit')): ?>
                    <?php echo e(Form::submit(Lang::get('messages.admin.manga.edit.update-manga'), array('class' => 'btn btn-primary btn-xs'))); ?>

                    <?php endif; ?>
                    <?php echo e(Form::hidden('cover', '', array('id' => 'mangacover'))); ?>

                </div>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <div class="col-md-3">
        <div id="coverContainer">
            <div class="coverWrapper">
                <div class="previewWrapper">
                    <?php if(!is_null($manga->cover)): ?>
                    <img class="img-responsive img-rounded" src='<?php echo e(HelperController::coverUrl("{$manga->slug}/cover/cover_250x350.jpg")); ?>' alt='<?php echo e($manga->name); ?>'>
                    <?php else: ?>
                    <i class="fa fa-file-image-o"></i>
                    <?php endif; ?>
                    <div id="previews">
                        <div id="previewTemplate">
                            <div class="dz-preview dz-file-preview">
                                <div class="dz-details">
                                    <div class="dz-filename"><?php echo e(Lang::get('messages.admin.manga.create.filename')); ?> <span data-dz-name></span></div>
                                    <div class="dz-size"><?php echo e(Lang::get('messages.admin.manga.create.size')); ?> <span data-dz-size></span></div>
                                    <img data-dz-thumbnail />
                                </div>
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if((Sentinel::check()->id==$manga->user->id && Sentinel::hasAccess('manage_my_manga')) || Sentinel::hasAccess('manga.manga.edit')): ?>
                <div class="uploadBtn">
                    <span class="btn btn-success fileinput-upload btn-xs dz-clickable ">
                        <i class="fa fa-plus"></i>
                        <span><?php echo e(Lang::get('messages.admin.manga.create.upload-cover')); ?></span>
                    </span>
                    <span class="btn btn-danger fileinput-remove btn-xs data-dz-remove disabled">
                        <i class="fa fa-times"></i>
                        <span><?php echo e(Lang::get('messages.admin.manga.create.remove-cover')); ?></span>
                    </span>
                </div>
                <?php endif; ?>

                <div class="dz-error-message" style="color: red"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // generate slug
        $('#name').keyup(function () {
            slug = $(this).val().toLowerCase()
                    .replace(/[^\w ]+/g, '')
                    .replace(/ +/g, '-');
            $('#slug').val(slug);
        });

        // tags
        var tags = "<?php echo e($tags); ?>";
        var data = tags.split(',');
        var items = data.map(function (x) {
            return {item: x};
        });

        $('.tags').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            options: items,
            labelField: "item",
            valueField: "item",
            sortField: 'item',
            searchField: 'item',
            create: true
        });
        
        // authors
        var authors = "<?php echo e($authors); ?>";
        var data = authors.split(',');
        var items2 = data.map(function (x) {
            return {item: x};
        });

        $('.authors').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            options: items2,
            labelField: "item",
            valueField: "item",
            sortField: 'item',
            searchField: 'item',
            create: true
        });

        <?php if(!is_null($manga->cover)): ?>
                $("span.fileinput-remove").removeClass("disabled");
        $("span.fileinput-upload").addClass("disabled");
        $('#mangacover').val('<?php echo e(HelperController::coverUrl("{$manga->slug}/cover/cover_250x350.jpg")); ?>');

        $("span.fileinput-remove").off().on("click", function () {
            $('.previewWrapper').find('img').remove();
            $('.previewWrapper').append('<i class="fa fa-file-image-o"></i>');
            $("span.fileinput-upload").removeClass("disabled");
            $("span.fileinput-remove").addClass("disabled");
            $('#mangacover').val("");
        });
        <?php else: ?> if($('#mangacover').val().length) {
        $(".previewWrapper i").remove();
        $("span.fileinput-upload").addClass("disabled");
        $("span.fileinput-remove").removeClass("disabled");
        $('.previewWrapper').append("<img class='img-responsive img-rounded' width='250' height='350' src='" + $('#mangacover').val() + "' />");
        $("span.fileinput-remove").off().on("click", function () {
            deletefile($('#mangacover').val().replace(/^.*[\\\/]/, ''));
        });
        }
        <?php endif; ?>
    });

    // Get the template HTML and remove it from the document
    var previewTemplate = $('#previews').html();
    $('#previewTemplate').remove();

    var myDropzone = new Dropzone("#coverContainer", {
        url: "<?php echo e(route('admin.upload.cover')); ?>",
        thumbnailWidth: 200,
        thumbnailHeight: 280,
        acceptedFiles: 'image/*',
        previewTemplate: previewTemplate,
        previewsContainer: "#previews", // Define the container to display the previews
        clickable: ".fileinput-upload" // Define the element that should be used as click trigger to select files.
    });

    myDropzone.on("sending", function (file, xhr, formData) {
        formData.append('_token', '<?php echo e(csrf_token()); ?>');

        $(".previewWrapper i").remove();
        $("span.fileinput-upload").addClass("disabled");
    });

    myDropzone.on("success", function (file, response) {
        $('#previewTemplate').remove();
        $('.previewWrapper').append("<img class='img-responsive img-rounded' width='250' height='350' src='" + response.result + "' />");
        $('#mangacover').val(response.result);
        $("span.fileinput-remove").removeClass("disabled");
        $("span.fileinput-remove").off().on("click", function () {
            <?php if(!is_null($manga->cover)): ?>
                    $('.previewWrapper').find('img').remove();
            $('.previewWrapper').append('<i class="fa fa-file-image-o"></i>');
            $("span.fileinput-upload").removeClass("disabled");
            $("span.fileinput-remove").addClass("disabled");
            $('#mangacover').val("");
            <?php else: ?>
                    deletefile($('#mangacover').val().replace(/^.*[\\\/]/, ''));
            <?php endif; ?>
        });
    });

    myDropzone.on("error", function (file, response) {
        $('.dz-error-message').html(response.error.type + ': ' + response.error.message);
    });

    function deletefile(value) {
        $.post(
                "<?php echo e(route('admin.delete.cover')); ?>",
                {filename: value, _token: '<?php echo e(csrf_token()); ?>'}, function () {
            $('.previewWrapper').find('img').remove();
            $('.previewWrapper').append('<i class="fa fa-file-image-o"></i>');
            $("span.fileinput-upload").removeClass("disabled");
            $("span.fileinput-remove").addClass("disabled");
            $('#mangacover').val("");
        });
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>