<?php $__env->startSection('title'); ?>
Reset Password
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="login-box">
    <div class="login-logo">
        <a href="<?php echo e(route('front.index')); ?>"><b><?php echo e($sitename); ?></b></a>
    </div>
    <?php echo $__env->make('base::admin._partials.notifications', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="login-box-body">
        <p class="login-box-msg"><?php echo e(trans('user::messages.front.auth.reset_password_body')); ?></p>

        <?php echo e(Form::open(['route' => 'reset.post'])); ?>

        <div class="form-group has-feedback <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
            <input type="email" class="form-control" autofocus
                   name="email" placeholder="<?php echo e(trans('user::messages.front.auth.email')); ?>" value="<?php echo e(old('email')); ?>">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <?php echo $errors->first('email', '<span class="help-block">:message</span>'); ?>

        </div>
        
        <?php if(isset($captcha->form_reset) && $captcha->form_reset === '1'): ?>
        <div class="form-group has-feedback <?php echo e($errors->has('g-recaptcha-response') ? ' has-error' : ''); ?>">
            <?php echo NoCaptcha::renderJs(); ?>

            <?php echo NoCaptcha::display(); ?>

            <?php echo $errors->first('g-recaptcha-response', '<span class="help-block">:message</span>'); ?>

        </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat pull-right">
                    <?php echo e(trans('user::messages.front.auth.reset_password')); ?>

                </button>
            </div>
        </div>
        <?php echo e(Form::close()); ?>


        <a href="<?php echo e(route('login')); ?>" class="text-center"><?php echo e(trans('user::messages.front.auth.i_remembered_password')); ?></a>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user::layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>