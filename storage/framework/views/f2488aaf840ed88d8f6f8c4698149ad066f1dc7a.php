

<?php $__env->startSection('breadcrumbs'); ?>
<?php echo Breadcrumbs::render('admin.manga.chapter.create', $manga); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-book"></i> <?php echo e(Lang::get('messages.admin.chapter.create.title')); ?>

            </div>
            <!-- /.panel-heading -->
            <?php echo e(Form::open(array('route' => ['admin.manga.chapter.store', $manga->id]))); ?>

            <div class="box-body">
                <div class="form-group">
                    <?php echo e(Form::label('name', Lang::get('messages.admin.chapter.create.chapter-name'))); ?>

                    <?php echo e(Form::text('name', '', array('class' => 'form-control'))); ?>

                    <?php echo $errors->first('name', '<label class="error" for="name">:message</label>'); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('number', Lang::get('messages.admin.chapter.create.number'))); ?>

                    <?php echo e(Form::text('number', '', array('class' => 'form-control'))); ?>

                    <?php echo $errors->first('number', '<label class="error" for="number">:message</label>'); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('slug', Lang::get('messages.admin.chapter.create.slug'))); ?>

                    <?php echo e(Form::text('slug', '', array('class' => 'form-control', 'placeholder' => Lang::get('messages.admin.chapter.create.slug-placeholder')))); ?>

                    <?php echo $errors->first('slug', '<label class="error" for="slug">:message</label>'); ?>

                </div>              

                <div class="form-group">
                    <?php echo e(Form::label('volume', Lang::get('messages.admin.chapter.create.volume'))); ?>

                    <?php echo e(Form::text('volume', '', array('class' => 'form-control'))); ?>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="pull-right">
                    <?php echo e(link_to_route('admin.manga.show', Lang::get('messages.admin.chapter.back'), $manga->id, array('class' => 'btn btn-default btn-xs'))); ?>


                    <?php if((Sentinel::check()->id==$manga->user->id && Sentinel::hasAccess('manage_my_chapters')) || Sentinel::hasAccess('manga.chapter.create')): ?>
                    <?php echo e(Form::submit(Lang::get('messages.admin.chapter.create.create-chapter'), array('class' => 'btn btn-primary btn-xs'))); ?>

                    <?php echo e(Form::hidden('mangaId', $manga->id, array('id' => 'mangaId'))); ?>

                    <?php endif; ?>
                </div>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>