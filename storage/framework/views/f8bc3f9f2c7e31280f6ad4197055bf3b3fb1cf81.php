<div class="type-content">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="filter-text pull-left"></div>
            <div class="pull-right">
                <?php echo e(Lang::get('messages.front.directory.sort-by')); ?>

                <div id="sort-types" class="btn-group" role="group" data-toggle="buttons">
                    <div class="btn-group" role="group">
                        <label class="btn btn-default active">
                            <input type="radio" name="sort-type" id="name" /> <?php echo e(Lang::get('messages.front.directory.az')); ?>

                        </label>
                    </div>
                    <div class="btn-group" role="group">
                        <label class="btn btn-default">
                            <input type="radio" name="sort-type" id="views" /> <?php echo e(Lang::get('messages.front.directory.views')); ?>

                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="content">
            <?php echo $__env->make('front.themes.'.$theme.'.blocs.manga.list.filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </div>
</div>