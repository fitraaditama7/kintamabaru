

<?php $__env->startSection('breadcrumbs'); ?>
<?php echo Breadcrumbs::render('admin.settings.seo'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="fa fa-google fa-fw"></i> <?php echo e(Lang::get('messages.admin.settings.seo.header')); ?>

                </h3>
            </div>

            <?php echo e(Form::open(array('route' => 'admin.settings.seo.save', 'role' => 'form'))); ?>

            <div class="box-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" style="margin: 10px -10px 15px;padding:0 10px">
                    <li role="presentation" class="active">
                        <a href="#general" aria-controls="general" role="tab" data-toggle="tab"><?php echo e(Lang::get('messages.admin.settings.seo.global')); ?></a>
                    </li>
                    <li role="presentation">
                        <a href="#advanced" aria-controls="advanced" role="tab" data-toggle="tab"><?php echo e(Lang::get('messages.admin.settings.seo.advanced')); ?></a>
                    </li>
                </ul>

                <div class="tab-content">
                    <!-- general -->
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <?php echo e(Form::label('seo.title', Lang::get('messages.admin.settings.seo.title'))); ?>

                            <?php echo e(Form::text('seo.title', $options['seo.title'], array('class' => 'form-control'))); ?>

                        </div>
                        <div class="form-group">
                            <?php echo e(Form::label('seo.keywords', Lang::get('messages.admin.settings.seo.keywords'))); ?>

                            <?php echo e(Form::text('seo.keywords', $options['seo.keywords'], array('class' => 'form-control', 'placeholder' => 'comma separated'))); ?>

                        </div>
                        <div class="form-group">
                            <?php echo e(Form::label('seo.description', Lang::get('messages.admin.settings.seo.description'))); ?>

                            <?php echo e(Form::text('seo.description', $options['seo.description'], array('class' => 'form-control'))); ?>

                        </div>
                        <div class="form-group">
                            <?php echo e(Form::label('seo.google.analytics', Lang::get('messages.admin.settings.seo.ga-id'))); ?>

                            <?php echo e(Form::text('seo.google.analytics', $options['seo.google.analytics'], ['class' => 'form-control'])); ?>

                        </div>
                        <div class="form-group">
                            <?php echo e(Form::label('seo.google.webmaster', Lang::get('messages.admin.settings.seo.gw-id'))); ?>

                            <?php echo e(Form::text('seo.google.webmaster', $options['seo.google.webmaster'], ['class' => 'form-control'])); ?>

                        </div>
                    </div>

                    <!-- advanced -->
                    <div role="tabpanel" class="tab-pane" id="advanced">
                        <!-- info -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo e(Lang::get('messages.admin.settings.seo.info-page')); ?>

                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12 col-sm-8">
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.title')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[info][title][global]" value="1" 
                                                   <?php if (isset($advanced->info->title->global) && $advanced->info->title->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[info][title][value]"
                                               value="<?php echo e($advanced->info->title->value); ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.description')); ?></label><?php echo e(Lang::get('messages.admin.settings.seo.desc-info')); ?> 
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[info][description][global]" value="1" 
                                                   <?php if (isset($advanced->info->description->global) && $advanced->info->description->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[info][description][value]"
                                               value="<?php echo e($advanced->info->description->value); ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.keywords')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[info][keywords][global]" value="1" 
                                                   <?php if (isset($advanced->info->keywords->global) && $advanced->info->keywords->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[info][keywords][value]"
                                               value="<?php echo e($advanced->info->keywords->value); ?>"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="well">
                                        <span><?php echo e(Lang::get('messages.admin.settings.seo.available-vars')); ?></span>
                                        <ul>
                                            <li>
                                                <b>%manga_name%</b>
                                            </li>
                                            <li>
                                                <b>%manga_author%</b>
                                            </li>
                                            <li>
                                                <b>%manga_artist%</b>
                                            </li>
                                            <li>
                                                <b>%manga_categories%</b>
                                            </li>
                                            <li>
                                                <b>%manga_description%</b>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- reader -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo e(Lang::get('messages.admin.settings.seo.reader-page')); ?>

                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12 col-sm-8">
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.title')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[reader][title][global]" value="1" 
                                                   <?php if (isset($advanced->reader->title->global) && $advanced->reader->title->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[reader][title][value]"
                                               value="<?php echo e($advanced->reader->title->value); ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.description')); ?></label><?php echo e(Lang::get('messages.admin.settings.seo.desc-info')); ?> 
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[reader][description][global]" value="1" 
                                                   <?php if (isset($advanced->reader->description->global) && $advanced->reader->description->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[reader][description][value]"
                                               value="<?php echo e($advanced->reader->description->value); ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.keywords')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[reader][keywords][global]" value="1" 
                                                   <?php if (isset($advanced->reader->keywords->global) && $advanced->reader->keywords->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[reader][keywords][value]"
                                               value="<?php echo e($advanced->reader->keywords->value); ?>"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="well">
                                        <span><?php echo e(Lang::get('messages.admin.settings.seo.available-vars.reader')); ?></span>
                                        <ul>
                                            <li>
                                                <b>%chapter_title%</b>
                                            </li>
                                            <li>
                                                <b>%chapter_number%</b>
                                            </li>
                                            <li>
                                                <b>%chapter_volume%</b>
                                            </li>
                                            <li>
                                                <b>%page_number%</b>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- news -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo e(Lang::get('messages.admin.settings.seo.news-page')); ?>

                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12 col-sm-8">
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.title')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[news][title][global]" value="1" 
                                                   <?php if (isset($advanced->news->title->global) && $advanced->news->title->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[news][title][value]"
                                               value="<?php echo e($advanced->news->title->value); ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.description')); ?></label><?php echo e(Lang::get('messages.admin.settings.seo.desc-info')); ?> 
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[news][description][global]" value="1" 
                                                   <?php if (isset($advanced->news->description->global) && $advanced->news->description->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[news][description][value]"
                                               value="<?php echo e($advanced->news->description->value); ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.keywords')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[news][keywords][global]" value="1" 
                                                   <?php if (isset($advanced->news->keywords->global) && $advanced->news->keywords->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[news][keywords][value]"
                                               value="<?php echo e($advanced->news->keywords->value); ?>"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="well">
                                        <span><?php echo e(Lang::get('messages.admin.settings.seo.available-vars.news')); ?></span>
                                        <ul>
                                            <li>
                                                <b>%post_title%</b>
                                            </li>
                                            <li>
                                                <b>%post_content%</b> 
                                            </li>
                                            <li>
                                                <b>%post_keywords%</b> 
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- manga list -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo e(Lang::get('messages.admin.settings.seo.mangalist-page')); ?>

                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.title')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[mangalist][title][global]" value="1" 
                                                   <?php if (isset($advanced->mangalist->title->global) && $advanced->mangalist->title->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[mangalist][title][value]"
                                               value="<?php if(isset($advanced->mangalist->title->value)): ?><?php echo e($advanced->mangalist->title->value); ?><?php endif; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.description')); ?></label><?php echo e(Lang::get('messages.admin.settings.seo.desc-info-max')); ?> 
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[mangalist][description][global]" value="1" 
                                                   <?php if (isset($advanced->mangalist->description->global) && $advanced->mangalist->description->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[mangalist][description][value]"
                                               value="<?php if(isset($advanced->mangalist->description->value)): ?><?php echo e($advanced->mangalist->description->value); ?><?php endif; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.keywords')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[mangalist][keywords][global]" value="1" 
                                                   <?php if (isset($advanced->mangalist->keywords->global) && $advanced->mangalist->keywords->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[mangalist][keywords][value]"
                                               value="<?php if(isset($advanced->mangalist->keywords->value)): ?><?php echo e($advanced->mangalist->keywords->value); ?><?php endif; ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- latest release -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo e(Lang::get('messages.admin.settings.seo.latestrelease-page')); ?>

                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.title')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[latestrelease][title][global]" value="1" 
                                                   <?php if (isset($advanced->latestrelease->title->global) && $advanced->latestrelease->title->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[latestrelease][title][value]"
                                               value="<?php if(isset($advanced->latestrelease->title->value)): ?><?php echo e($advanced->latestrelease->title->value); ?><?php endif; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.description')); ?></label><?php echo e(Lang::get('messages.admin.settings.seo.desc-info-max')); ?> 
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[latestrelease][description][global]" value="1" 
                                                   <?php if (isset($advanced->latestrelease->description->global) && $advanced->latestrelease->description->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[latestrelease][description][value]"
                                               value="<?php if(isset($advanced->latestrelease->description->value)): ?><?php echo e($advanced->latestrelease->description->value); ?><?php endif; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.keywords')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[latestrelease][keywords][global]" value="1" 
                                                   <?php if (isset($advanced->latestrelease->keywords->global) && $advanced->latestrelease->keywords->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[latestrelease][keywords][value]"
                                               value="<?php if(isset($advanced->latestrelease->keywords->value)): ?><?php echo e($advanced->latestrelease->keywords->value); ?><?php endif; ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- latest news -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <?php echo e(Lang::get('messages.admin.settings.seo.latestnews-page')); ?>

                            </div>
                            <div class="panel-body">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.title')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[latestnews][title][global]" value="1" 
                                                   <?php if (isset($advanced->latestnews->title->global) && $advanced->latestnews->title->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[latestnews][title][value]"
                                               value="<?php if(isset($advanced->latestnews->title->value)): ?><?php echo e($advanced->latestnews->title->value); ?><?php endif; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.description')); ?></label><?php echo e(Lang::get('messages.admin.settings.seo.desc-info-max')); ?> 
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[latestnews][description][global]" value="1" 
                                                   <?php if (isset($advanced->latestnews->description->global) && $advanced->latestnews->description->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[latestnews][description][value]"
                                               value="<?php if(isset($advanced->latestnews->description->value)): ?><?php echo e($advanced->latestnews->description->value); ?><?php endif; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo e(Lang::get('messages.admin.settings.seo.keywords')); ?></label>
                                        <div class="pull-right">
                                            <input type="checkbox" name="seo.advanced[latestnews][keywords][global]" value="1" 
                                                   <?php if (isset($advanced->latestnews->keywords->global) && $advanced->latestnews->keywords->global == '1') { ?> checked="checked" <?php } ?> />
                                            <?php echo e(Lang::get('messages.admin.settings.seo.use-global')); ?>

                                        </div>
                                        <input type="text" class="form-control" name="seo.advanced[latestnews][keywords][value]"
                                               value="<?php if(isset($advanced->latestnews->keywords->value)): ?><?php echo e($advanced->latestnews->keywords->value); ?><?php endif; ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <?php echo e(Form::submit(Lang::get('messages.admin.settings.save'), ['class' => 'btn btn-primary pull-right'])); ?>

                    </div>
                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>