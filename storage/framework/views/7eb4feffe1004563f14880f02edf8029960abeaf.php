<?php if(count($filter)>0): ?>
<?php $__currentLoopData = $filter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $manga): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="col-sm-6">
    <div class="media" style="margin-bottom: 20px;">
        <div class="media-left">
            <a href="<?php echo e(route('front.manga.show',$manga->slug)); ?>" class="thumbnail">
                <img width="100" src='<?php echo e(HelperController::coverUrl("$manga->slug/cover/cover_250x350.jpg")); ?>' alt='<?php echo e($manga->name); ?>'>
            </a>
        </div>
        <div class="media-body">
            <?php $rate = Jraty::get($manga->id); ?>
            <h5 class="media-heading"><a href="<?php echo e(route('front.manga.show',$manga->slug)); ?>" class="chart-title"><strong><?php echo e($manga->name); ?></strong></a></h5>
            <div class="readOnly-<?php echo e($manga->id); ?>" style="display: inline-block;"></div> <span style="vertical-align: middle;"><?php echo e($rate->avg); ?></span>
            <script>$('.readOnly-<?php echo e($manga->id); ?>').raty({path: "<?php echo e(asset('/packages/escapeboy/jraty/raty/lib/img')); ?>", readOnly: true, score: "<?php echo e($rate->avg); ?>"});</script>
            <div>
                <i class="fa fa-eye"></i> <?php echo e($manga->views); ?>

            </div>
            <?php if(count($manga->categories)>0): ?>
            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 95%;">
                <?php echo e(App::make("HelperController")->listAsString($manga->categories, ', ')); ?>

            </div>
            <?php endif; ?>
            <?php $lastChapter = $manga->lastChapter(); ?>
            <?php if(!is_null($lastChapter)): ?>
            <div style="position: absolute; bottom: 20px">
                <?php echo e(link_to_route('front.manga.reader', "#".$lastChapter->number." ".$lastChapter->name, [$manga->slug, $lastChapter->slug])); ?>

            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<div class="row">
    <div class="col-xs-12">
        <?php echo e($filter->links()); ?>

    </div>
</div>
<?php else: ?>
<div class="center-block">
    <p><?php echo e(Lang::get('messages.front.directory.no-manga')); ?></p>
</div>
<?php endif; ?>