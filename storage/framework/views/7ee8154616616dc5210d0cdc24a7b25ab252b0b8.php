

<?php $__env->startSection('title'); ?>
<?php if(isset($seo->info->title->global) && $seo->info->title->global == '1'): ?>
<?php echo e($settings['seo.title']); ?> | <?php echo e($manga->name); ?>

<?php else: ?>
<?php echo e(App::make("HelperController")->advSeoInfoPage($seo->info->title->value, $manga)); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
<?php if(isset($seo->info->description->global) && $seo->info->description->global == '1'): ?>
<?php echo e($settings['seo.description']); ?>

<?php else: ?>
<?php echo e(App::make("HelperController")->advSeoInfoPage($seo->info->description->value, $manga)); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('keywords'); ?>
<?php if(isset($seo->info->keywords->global) && $seo->info->keywords->global == '1'): ?>
<?php echo e($settings['seo.keywords']); ?>

<?php else: ?>
<?php echo e(App::make("HelperController")->advSeoInfoPage($seo->info->keywords->value, $manga)); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.themes.'.$theme.'.blocs.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startSection('header'); ?>
<?php echo Jraty::js(); ?>


<?php echo Jraty::js_init(array(
'score' => 'function() { return $(this).attr(\'data-score\'); }',
'number' => 5,
'click' => 'function(score, evt) {
$.post("'.URL::to('/').'/save/item_rating",{
item_id: $(\'[data-item]\').attr(\'data-item\'),
score: score
});
}',
'path' => "'".asset('/packages/escapeboy/jraty/raty/lib/img')."'"
)); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('allpage'); ?>
<h2 class="widget-title" style="display: inline-block;"><?php echo e($manga->name); ?></h2>
<?php if (is_module_enabled('MySpace')): ?>
<?php if(Sentinel::check()): ?>
<span class="bookmark <?php if(config('settings.orientation') === 'rtl'): ?> pull-left <?php else: ?> pull-right <?php endif; ?>" style="; display: inline-block; margin: 21px 0px 10.5px;">
    <a href="#"><i class="fa fa-heart" style="color: red"></i> <?php echo e(Lang::get('messages.front.bookmarks.bookmark')); ?></a>
</span>

<script>
    $('.bookmark a').click(function (e) {
        e.preventDefault();

        $.ajax({
            url: "<?php echo e(route('bookmark.store')); ?>",
            method: 'POST',
            data: {
                'manga_id': '<?php echo e($manga->id); ?>',
                'chapter_id': '0',
                'page_slug': '0',
                '_token': '<?php echo e(csrf_token()); ?>'
            },
            success: function (response) {
                if (response.status == 'ok') {
                    alert("<?php echo e(Lang::get('messages.front.bookmarks.bookmarked')); ?>");
                }
            },
            error: function (response) {
                alert("<?php echo e(Lang::get('messages.front.bookmarks.error')); ?>");
            }
        });
    });
</script>
<?php endif; ?>
<?php endif; ?>
<hr/>

<div class="row">
    <div class="col-sm-4">
        <div class="boxed" style="width: 250px; height: 350px;">
            <?php if($manga->cover): ?>
            <img class="img-responsive" src='<?php echo e(HelperController::coverUrl("$manga->slug/cover/cover_250x350.jpg")); ?>' alt='<?php echo e($manga->name); ?>'/>
            <?php else: ?>
            <img width="250" height="350" src='<?php echo e(asset("images/no-image.png")); ?>' alt='<?php echo e($manga->name); ?>' />
            <?php endif; ?>
        </div>
    </div>
    <div class="col-sm-8">
        <dl class="dl-horizontal">
            <?php if(!is_null($manga->type)): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.type')); ?></dt>
            <dd>
                <?php echo e($manga->type->label); ?>

            </dd>
            <?php endif; ?>
            
            <?php if(!is_null($manga->status)): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.status')); ?></dt>
            <dd>
                <?php if($manga->status->id == 1): ?>
                <span class="label label-success"><?php echo e($manga->status->label); ?></span>
                <?php else: ?>
                <span class="label label-danger"><?php echo e($manga->status->label); ?></span>
                <?php endif; ?>          
            </dd>
            <?php endif; ?>

            <?php if(!is_null($manga->otherNames) && $manga->otherNames != ""): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.other-names')); ?></dt>
            <dd><?php echo e($manga->otherNames); ?></dd>
            <?php endif; ?>

            <?php if(count($manga->authors)>0): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.author')); ?></dt>
            <dd>
                <?php $__currentLoopData = $manga->authors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$author): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo e(link_to_route('front.manga.list.archive', trim($author->name), ['author', trim($author->name)])); ?>

                <?php if($index!=count($manga->authors)-1): ?>
                ,&nbsp;
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </dd>
            <?php endif; ?>

            <?php if(count($manga->artists)>0): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.artist')); ?></dt>
            <dd>
                <?php $__currentLoopData = $manga->artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo e(link_to_route('front.manga.list.archive', trim($artist->name), ['artist', trim($artist->name)])); ?>

                <?php if($index!=count($manga->artists)-1): ?>
                ,&nbsp;
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </dd>
            <?php endif; ?>

            <?php if(!is_null($manga->releaseDate) && $manga->releaseDate != ""): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.released')); ?></dt>
            <dd><?php echo e($manga->releaseDate); ?></dd>
            <?php endif; ?>

            <?php if(count($manga->categories)>0): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.categories')); ?></dt>
            <dd>
                <?php $__currentLoopData = $manga->categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo e(link_to_route('front.manga.list.archive', $category->name, ['category', $category->slug])); ?>

                <?php if($index!=count($manga->categories)-1): ?>
                ,&nbsp;
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </dd>
            <?php endif; ?>

            <?php if(count($manga->tags)>0): ?>
            <dt><?php echo e(Lang::get('messages.front.manga.tags')); ?></dt>
            <dd class="tag-links">
                <?php $__currentLoopData = $manga->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo e(link_to_route('front.manga.list.archive', $tag->name, ['tag', $tag->slug])); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </dd>
            <?php endif; ?>
            
            <br/>

            <dt><?php echo e(Lang::get('messages.front.directory.views')); ?></dt>
            <dd><?php echo e($manga->views); ?></dd>

            <dt><?php echo e(Lang::get('messages.front.manga.rating')); ?></dt>
            <dd>
                <div class="rating clearfix">
                    <?php echo Jraty::html($manga->id, $manga->name, HelperController::coverUrl("$manga->slug/cover/cover_250x350.jpg"), $seo = true); ?>
                    <?php $rating = Jraty::get($manga->id) ?>
                    <?php echo e(Lang::get('messages.front.manga.note', array('avg' => $rating->avg, 'votes' => $rating->votes))); ?>

                </div>
            </dd>
        </dl>

        <?php if($manga->caution == 1): ?>
        <div role="alert" class="alert alert-danger" style="margin: 10px;">
            <?php echo e(Lang::get('messages.front.manga.caution')); ?>

        </div>
        <?php endif; ?>
    </div>
</div>
<?php if(!is_null($manga->summary) && $manga->summary != ""): ?>
<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="well">
            <h5><strong><?php echo e(Lang::get('messages.front.manga.summary')); ?></strong></h5> 
            <p><?php echo e($manga->summary); ?></p>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if(count($posts)>0): ?>
<div class="row">
    <div class="col-lg-12">
        <h2 class="widget-title"><?php echo e(Lang::get('messages.front.home.news')); ?></h2> 
        <hr/>
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="news-item pull-right" style="display: inline-block; width: 100%;">
            <h3 class="manga-heading pull-left">
                <i class="fa fa-square"></i>
                <a href="<?php echo e(route('front.news', $post->slug)); ?>"><?php echo e($post->title); ?></a>
            </h3>
            <div class="pull-right" style="font-size: 13px;">
                <span class="pull-left">
                    <i class="fa fa-clock-o"></i> <?php echo e(App::make("HelperController")->formateCreationDate($post->created_at)); ?>&nbsp;&middot;&nbsp;
                </span>
                <span class="pull-left"><i class="fa fa-user"></i> <?php echo e($post->user->username); ?></span>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<br/>
<?php endif; ?>

<!-- ads -->
<div class="row">
    <div class="col-xs-12" style="padding: 0">
        <div class="ads-large" style="display: table; margin: 10px auto;">
            <?php echo isset($ads['TOP_LARGE'])?$ads['TOP_LARGE']:''; ?>

        </div>
        <div style="display: table; margin: 10px auto;">
            <div class="pull-left ads-sqre1" style="margin-right: 50px;">
                <?php echo isset($ads['TOP_SQRE_1'])?$ads['TOP_SQRE_1']:''; ?>

            </div>
            <div class="pull-right ads-sqre2">
                <?php echo isset($ads['TOP_SQRE_2'])?$ads['TOP_SQRE_2']:''; ?>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h2 class="widget-title"><?php echo e(Lang::get('messages.front.manga.chapters', array('manganame' => $manga->name))); ?></h2> 
        <hr/>

        <ul class="chapters">
            <?php if(count($chapters)>0): ?>
            <?php $volume = 0; ?>
            <?php $__currentLoopData = $chapters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chapter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(isset($mangaOptions->show_chapters_volume) && $mangaOptions->show_chapters_volume == '1'): ?>
            <?php if($volume!=$chapter->volume): ?>
            <li class="volume btn btn-default btn-xs" data-volume="volume-<?php echo e($chapter->volume); ?>">
                <i class="fa fa-minus-square-o"></i> Volume <?php echo e($chapter->volume); ?>

            </li>
            <?php endif; ?>
            <?php $volume = $chapter->volume; ?>
            <?php endif; ?>

            <li style="padding: 3px 0;" class="volume-<?php echo e($chapter->volume); ?>">
                <h5 class="chapter-title-rtl">
                    <?php echo e(link_to_route('front.manga.reader', $manga->name.' '.$chapter->number, [$manga->slug, $chapter->slug])); ?> : 
                    <em><?php echo e($chapter->name); ?></em>
                </h5>
                <div class="action <?php if(config('settings.orientation') === 'rtl'): ?> pull-left <?php endif; ?>">
                    <?php if (isset($mangaOptions->allow_download_chapter) && $mangaOptions->allow_download_chapter == '1') { ?>
                        <?php if(config('settings.orientation') === 'rtl'): ?>
                        <?php echo e(link_to_route('front.manga.download', Lang::get('messages.front.manga.download'), array('mangaSlug' => $manga->slug, 'chapterId' => $chapter->id),
                                    array('class' => 'btn btn-primary btn-xs download', 'style' => 'float:left; margin-right: 10%'))); ?>

                        <?php else: ?>
                        <?php echo e(link_to_route('front.manga.download', Lang::get('messages.front.manga.download'), array('mangaSlug' => $manga->slug, 'chapterId' => $chapter->id),
                                    array('class' => 'btn btn-primary btn-xs download', 'style' => 'float:right; margin-left: 10%'))); ?>

                        <?php endif; ?>
                    <?php } ?>
                    <div style="float:right" class="date-chapter-title-rtl">
                        <?php echo e(App::make("HelperController")->formateCreationDate($chapter->created_at)); ?>

                    </div>
                    <?php if (isset($mangaOptions->show_contributer_pseudo) && $mangaOptions->show_contributer_pseudo == '1') { ?>
                        <div <?php if(config('settings.orientation') === 'rtl'): ?> style="float: left; margin-left: 10%;"  <?php else: ?> style="float:right; margin-right: 10%" <?php endif; ?>>
                            <?php if (is_module_enabled('MySpace')): ?>
                                <a href="<?php echo e(route('user.show', $chapter->user->username)); ?>">
                                    <i class="fa fa-user"></i> <?php echo e($chapter->user->username); ?>

                                </a>
                            <?php else : ?>
                                <i class="fa fa-user"></i> <?php echo e($chapter->user->username); ?>

                            <?php endif; ?>
                        </div>
                    <?php } ?>
                </div>
            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <div class="center-block">
                <p><?php echo e(Lang::get('messages.front.manga.no-chapter')); ?></p>
            </div>
            <?php endif; ?>
        </ul>
    </div>
</div>

<!-- ads -->
<div class="row">
    <div class="col-xs-12" style="padding: 0">
        <div class="ads-large" style="display: table; margin: 10px auto;">
            <?php echo isset($ads['BOTTOM_LARGE'])?$ads['BOTTOM_LARGE']:''; ?>

        </div>
        <div style="display: table; margin: 10px auto;">
            <div class="pull-left ads-sqre1" style="margin-right: 50px;">
                <?php echo isset($ads['BOTTOM_SQRE_1'])?$ads['BOTTOM_SQRE_1']:''; ?>

            </div>
            <div class="pull-right ads-sqre2">
                <?php echo isset($ads['BOTTOM_SQRE_2'])?$ads['BOTTOM_SQRE_2']:''; ?>

            </div>
        </div>
    </div>
</div>

<?php if (is_module_enabled('MySpace')): ?>
<!-- comment -->
<input type="hidden" id="post_id" name="post_id" value="<?php echo e($manga->id); ?>"/>
<input type="hidden" id="post_type" name="post_type" value="manga"/>

<?php $comment = json_decode($settings['site.comment']) ?>

<?php if(isset($comment->page->mangapage) && $comment->page->mangapage == '1'): ?>
<div class="hrule"><br/></div>

<?php if(isset($comment->fb) && $comment->fb == '1'): ?>
<div id="fb-root"></div>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<?php endif; ?>

<div class="row" style="margin:15px auto;">
    <div class="col-xs-12">
        <ul class="nav nav-tabs" role="tablist">
            <?php if(isset($comment->builtin) && $comment->builtin == '1'): ?>
            <li role="presentation" class="active"><a href="#builtin" aria-controls="builtin" role="tab" data-toggle="tab"><?php echo e(Lang::get('messages.front.home.comment.builtin-tab')); ?></a></li>
            <?php endif; ?>
            <?php if(isset($comment->fb) && $comment->fb == '1'): ?>
            <li role="presentation"><a href="#fb" aria-controls="fb" role="tab" data-toggle="tab">Facebook</a></li>
            <?php endif; ?>
            <?php if(isset($comment->disqus) && $comment->disqus == '1'): ?>
            <li role="presentation"><a href="#disqus" aria-controls="disqus" role="tab" data-toggle="tab">Disqus</a></li>
            <?php endif; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php if(isset($comment->builtin) && $comment->builtin == '1'): ?>
            <div role="tabpanel" class="tab-pane active" id="builtin">
                <?php echo $__env->make('front.themes.'.$theme.'.blocs.comments', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <?php endif; ?>
            <?php if(isset($comment->fb) && $comment->fb == '1'): ?>
            <div role="tabpanel" class="tab-pane" id="fb">
                <div class="fb-comments" data-href="<?php echo e(route('front.manga.show', $manga->slug)); ?>" data-width="100%" data-numposts="5">
                </div>
            </div>
            <?php endif; ?>
            <?php if(isset($comment->disqus) && $comment->disqus == '1'): ?>
            <div role="tabpanel" class="tab-pane <?php if (!isset($comment->fb)) echo 'active'; ?>" id="disqus">
                <div id="disqus_thread"></div>
                <script>
                    var disqus_config = function () {
                        this.page.url = "<?php echo e(route('front.manga.show', $manga->slug)); ?>";
                    };

                    (function () {  // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');

                        s.src = '//<?php echo isset($comment->disqusUrl) ? $comment->disqusUrl : '' ?>/embed.js';

                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php endif; ?>
<?php endif; ?>

<?php if(Session::has('downloadError')): ?>
<script>
    alert('Sorry! thers is no pages on this chapter.');
</script>
<?php endif; ?>

<script>
    $(document).ready(function () {
        $(".volume").click(function () {
            volume = $(this).data('volume');
            $('li.' + volume).toggle();
            $(this).find('i').toggleClass('fa-minus-square-o')
                    .toggleClass('fa-plus-square-o');
        });
        
        $(".download").click(function () {
            //$(this).attr('disabled', 'disabled');
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>