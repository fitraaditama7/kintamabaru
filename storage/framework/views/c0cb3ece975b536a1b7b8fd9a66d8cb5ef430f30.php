<?php $__env->startSection('head'); ?>
<link rel="stylesheet" href="<?php echo e(asset('vendor/datatables/jquery.dataTables.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('vendor/datatables/buttons.dataTables.min.css')); ?>">

<script src="<?php echo e(asset('vendor/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/datatables/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('vendor/datatables/buttons.server-side.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
<?php echo Breadcrumbs::render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="icon-layers font-dark"></i>
                    <?php echo e(trans('base::messages.admin.modules')); ?>

                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php echo $dataTable->table(); ?>

            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

<?php echo $dataTable->scripts(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>