

<?php $__env->startSection('head'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-select.min.css')); ?>">
<script src="<?php echo e(asset('js/vendor/bootstrap-select.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
<?php echo Breadcrumbs::render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="fa fa-plus-square-o"></i> <?php echo e(Lang::get('user::messages.admin.settings.subscription_options')); ?>

                </h3>
            </div>
            <?php echo e(Form::open(array('route' => 'admin.settings.subscription.post'))); ?>

            <div class="box-body">
                <fieldset>
                    <legend style="padding-left: 20px">Subscription</legend>
                    <div class="row">
                        <div class="col-md-4"><label><?php echo e(Lang::get('messages.admin.users.options.allo-subscribe')); ?></label></div>
                        <div class="col-md-8">
                            <label class="radio-inline">
                                <input type="radio" name="subscribe" value="true" <?php if ($subscription->subscribe === 'true'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.yes')); ?></label>
                            <label class="radio-inline">
                                <input type="radio" name="subscribe" value="false" <?php if ($subscription->subscribe === 'false'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.no')); ?></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"><label><?php echo e(Lang::get('messages.admin.users.options.admin-activate-it')); ?></label></div>
                        <div class="col-md-8">
                            <label class="radio-inline">
                                <input type="radio" name="admin_confirm" value="true" <?php if ($subscription->admin_confirm === 'true'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.yes')); ?></label>
                            <label class="radio-inline">
                                <input type="radio" name="admin_confirm" value="false" <?php if ($subscription->admin_confirm === 'false'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.no')); ?></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"><label><?php echo e(Lang::get('messages.admin.users.options.send-confim-email')); ?></label></div>
                        <div class="col-md-8">
                            <label class="radio-inline">
                                <input type="radio" name="email_confirm" value="true" <?php if ($subscription->email_confirm === 'true'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.yes')); ?></label>
                            <label class="radio-inline">
                                <input type="radio" name="email_confirm" value="false" <?php if ($subscription->email_confirm === 'false'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.no')); ?></label>
                        </div>
                    </div>
                    <div class="help-block"><?php echo e(Lang::get('messages.admin.users.subscribe.hint')); ?></div>
                    <br/>
                    <div class="form-group">
                        <?php echo e(Form::label('default_role', Lang::get('messages.admin.users.options.default-role'))); ?>

                        <select name="default_role" class="selectpicker" data-width="100%">
                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </fieldset>
                <fieldset>
                    <legend style="padding-left: 20px">Email</legend>        
                    <div id="mailing" class="form-group">
                        <div class="form-group">
                            <label><?php echo e(Lang::get('messages.admin.users.options.address')); ?></label>
                            <input type="text" name="address" value="<?php echo e($subscription->address); ?>" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label><?php echo e(Lang::get('messages.admin.users.options.name')); ?></label>
                            <input type="text" name="name" value="<?php echo e($subscription->name); ?>" class="form-control"/>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="mailing" value="sendmail" <?php if ($subscription->mailing === 'sendmail'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.use-php-mail')); ?></label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="mailing" value="smtp" <?php if ($subscription->mailing === 'smtp'): ?>
                                       checked="checked"<?php endif ?>/><?php echo e(Lang::get('messages.admin.users.options.config-smtp')); ?></label>
                        </div>

                        <div id="smtp-conf" style="display: none;">
                            <div class="form-group">
                                <label><?php echo e(Lang::get('messages.admin.users.options.host')); ?></label>
                                <input type="text" name="host" value="<?php echo e($subscription->host); ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label><?php echo e(Lang::get('messages.admin.users.options.port')); ?></label>
                                <input type="text" name="port" value="<?php echo e($subscription->port); ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label><?php echo e(Lang::get('user::messages.admin.settings.encryption_protocol')); ?></label>
                                <input type="text" name="encryption" placeholder="ssl or tls" value="<?php echo e(isset($subscription->encryption)?$subscription->encryption:'tls'); ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label><?php echo e(Lang::get('messages.admin.users.options.username')); ?></label>
                                <input type="text" name="username" value="<?php echo e($subscription->username); ?>" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label><?php echo e(Lang::get('messages.admin.users.options.password')); ?></label>
                                <input type="password" name="password" value="<?php echo e($subscription->password); ?>" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <?php echo e(Form::submit(Lang::get('messages.admin.settings.update'), array('class' => 'btn btn-primary center-block save', 'style' => 'width: 100%'))); ?>

                </div>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.selectpicker').val("<?php echo e($subscription->default_role); ?>");
        smtpChecked();
    });

    $('#mailing input[type="radio"]').change(function () {
        smtpChecked();
    });

    function smtpChecked() {
        if ($('#mailing input[value="smtp"]').is(':checked')) {
            $('#smtp-conf').show();
        } else {
            $('#smtp-conf').hide();
        }
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>