<?php if(isset($seo->reader->description->global) && $seo->reader->description->global == '1'): ?>
<?php $description = $settings['seo.description'] ?>
<?php else: ?>
<?php $description = App::make("HelperController")->advSeoReaderPage($seo->reader->description->value, $current, is_null($page)?1:$page) ?>
<?php endif; ?>

<?php if(isset($seo->reader->keywords->global) && $seo->reader->keywords->global == '1'): ?>
<?php $keywords = $settings['seo.keywords'] ?>
<?php else: ?>
<?php $keywords = App::make("HelperController")->advSeoReaderPage($seo->info->keywords->value, $current, is_null($page)?1:$page) ?>
<?php endif; ?>

<!doctype html>
<!--[if lt IE 8 ]><html lang="<?php echo e(App::getLocale()); ?>" class="ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="<?php echo e(App::getLocale()); ?>" class="ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="<?php echo e(App::getLocale()); ?>" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="<?php echo e(App::getLocale()); ?>"> <!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <title>
            <?php if(isset($seo->reader->title->global) && $seo->reader->title->global == '1'): ?>
            <?php echo e($settings['seo.title']); ?> | <?php echo e($current->manga_name); ?> <?php echo e(' #'.$current->chapter_number.': '. $current->chapter_name); ?> <?php if(!is_null($page)): ?> - <?php echo e(Lang::get('messages.front.reader.page')); ?> <?php endif; ?>
            <?php else: ?>
            <?php echo e(App::make("HelperController")->advSeoReaderPage($seo->reader->title->value, $current, is_null($page)?1:$page)); ?>

            <?php endif; ?>
        </title>
        <meta name="description" content="<?php echo $description ?>"/>
        <meta name="keywords" content="<?php echo $keywords ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <?php if(!is_null($settings['seo.google.webmaster']) || "" !== $settings['seo.google.webmaster']): ?>        
        <meta name="google-site-verification" content="<?php echo e($settings['seo.google.webmaster']); ?>" />
        <?php endif; ?>

        <?php if(!is_null($settings['site.theme.options']) || "" !== $settings['site.theme.options']): ?>
        <?php  $themeOpts=json_decode($settings['site.theme.options'])  ?>
        <?php if(!is_null($themeOpts->icon)): ?>
        <link rel="shortcut icon" href="<?php echo e($themeOpts->icon); ?>">
        <?php endif; ?>
        <?php endif; ?>
        
        <link rel="stylesheet" href="<?php echo e(asset('css/bootswatch/'.$readerTheme.'/bootstrap.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/reader.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-select.min.css')); ?>">

        <script src="<?php echo e(asset('js/vendor/jquery-1.11.0.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/vendor/bootstrap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/vendor/bootstrap-select.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/vendor/jquery.unveil.js')); ?>"></script>
        <script src="<?php echo e(asset('js/vendor/jquery.plugins.js')); ?>"></script>

        <?php if(config('settings.orientation') === 'rtl'): ?>
        <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-rtl.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/rtl.css')); ?>">
        <?php endif; ?>

        <!--[if lt IE 9]>
        <script src="<?php echo e(asset('js/vendor/html5shiv.js')); ?>"></script>
        <script src="<?php echo e(asset('js/vendor/respond.min.js')); ?>"></script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php $comment = json_decode($settings['site.comment']) ?>

        <?php if(isset($comment->page->reader) && $comment->page->reader == '1'): ?>
        <?php if(isset($comment->fb) && $comment->fb == '1'): ?>
        <div id="fb-root"></div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <?php endif; ?>
        <?php endif; ?>

        <?php if(!is_null($settings['seo.google.analytics']) || "" !== $settings['seo.google.analytics']): ?>
        <?php echo $__env->make('front.analyticstracking', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>

        <!-- Website Menu -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h2 class="<?php if(!is_null($themeOpts) && !is_null($themeOpts->logo)): ?>navbar-brand-logo <?php endif; ?>" style="margin:0;">
                        <a class="navbar-brand" href="<?php echo e(route('front.index')); ?>">
                            <?php if(!is_null($themeOpts) && !is_null($themeOpts->logo)): ?>
                            <img alt="<?php echo e($settings['site.name']); ?>" src="<?php echo e($themeOpts->logo); ?>"/>
                            <span style="display: none"><?php echo e($settings['site.name']); ?></span>
                            <?php else: ?>
                            <?php echo e($settings['site.name']); ?>

                            <?php endif; ?>
                        </a>
                    </h2>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav <?php if(config('settings.orientation') === 'rtl'): ?> navbar-left <?php else: ?> navbar-right <?php endif; ?>">
                        <?php if(env('ALLOW_SUBSCRIBE', false)): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="caret"></span></a>
                            <ul class="dropdown-menu profil-menu">
                                <?php if(!Sentinel::check()): ?>
                                <li>
                                    <a href="<?php echo e(route('register')); ?>">
                                        <i class="fa fa-pencil-square-o"></i> <?php echo e(Lang::get('messages.front.home.subscribe')); ?>

                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo e(route('login')); ?>">
                                        <i class="fa fa-sign-in"></i> <?php echo e(Lang::get('messages.front.home.login')); ?>

                                    </a>
                                </li>
                                <?php else: ?>
                                <li class="text-center" style="padding: 5px 0">
                                    Hi, <?php echo e($userCmp->username); ?>!
                                </li>
                                <?php if (is_module_enabled('MySpace')): ?>
                                <li>
                                    <a href="<?php echo e(route('user.show', $userCmp->username)); ?>">
                                        <i class="fa fa-user"></i> <?php echo e(Lang::get('messages.front.myprofil.my-profil')); ?>

                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo e(route('bookmark.index')); ?>">
                                        <i class="fa fa-heart"></i> <?php echo e(Lang::get('messages.front.bookmarks.title')); ?>

                                    </a>
                                </li>
                                <?php endif; ?>
                                <?php if (is_module_enabled('Notification')): ?>
                                    <li>
                                        <a href="<?php echo e(route('front.notification.index')); ?>">
                                            <i class="fa fa-bell"></i> My Notifications
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (is_module_enabled('Notification') || is_module_enabled('MySpace')): ?>
                                    <li role="separator" class="divider"></li>
                                <?php endif; ?>
                                <?php if (is_module_enabled('Manga')): ?>
                                    <?php if(Sentinel::hasAnyAccess(['manga.manga.create','manga.chapter.create'])): ?>
                                    <li>
                                        <a href="<?php echo e(route('admin.manga.index')); ?>">
                                            <i class="fa fa-plus"></i> <?php echo e(Lang::get('messages.front.myprofil.add-manga-chapter')); ?>

                                        </a>
                                    </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if (is_module_enabled('Blog')): ?>
                                    <?php if(Sentinel::hasAccess('blog.manage_posts')): ?>
                                    <li>
                                        <a href="<?php echo e(route('admin.posts.index')); ?>">
                                            <i class="fa fa-plus"></i> <?php echo e(Lang::get('messages.front.myprofil.add-post')); ?>

                                        </a>
                                    </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if(Sentinel::hasAccess('dashboard.index')): ?>
                                <li>
                                    <a href="<?php echo e(route('admin.index')); ?>">
                                        <i class="fa fa-cogs"></i> <?php echo e(Lang::get('messages.front.home.dashboard')); ?>

                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?php echo e(route('logout')); ?>">
                                        <i class="fa fa-sign-out"></i> <?php echo e(Lang::get('messages.front.home.logout')); ?>

                                    </a>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php if (is_module_enabled('MySpace')): ?>
                        <?php if(Sentinel::check()): ?>
                        <li>
                            <a href="" class="bookmark" title="<?php echo e(Lang::get('messages.front.bookmarks.bookmark')); ?>"><i class="fa fa-heart"></i></a>
                        </li>
                        <?php endif; ?>
                        <?php endif; ?>
                        <!-- Notifications Menu -->
                        <?php if (is_module_enabled('Notification')): ?>
                            <?php if(Sentinel::check()): ?>
                                <?php if (is_module_enabled('Notification')): ?>
                                    <?php echo $__env->make('notification::partials.notifications', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <li>
                            <!-- Button report bug -->
                            <a href="" class="btn-lg" data-toggle="modal" data-target="#myModal" title="<?php echo e(Lang::get('messages.front.reader.report-broken-image')); ?>">
                                <i class="fa fa-bug"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo e(route('front.manga.show', array($current->manga_slug))); ?>"><?php echo e($current->manga_name.' Manga'); ?></a></li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo e(Lang::get('messages.front.reader.reading-mode')); ?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a id="modePPP" href="#"><i class="fa fa-arrows-h" style="width: 30px"></i><?php echo e(Lang::get('messages.front.reader.page-per-page')); ?></a></li>
                                <li><a id="modeALL" href="#"><i class="fa fa-arrows-v" style="width: 30px"></i><?php echo e(Lang::get('messages.front.reader.all-pages')); ?></a></li>
                            </ul>
                        </li>

                        <li id="chapter-list" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo e(Lang::get('messages.front.reader.other-chapter')); ?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <?php $__currentLoopData = $chapters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chapter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li <?php if($chapter->chapter_number == $current->chapter_number): ?> class="active" <?php endif; ?>><a href="<?php echo e(route('front.manga.reader', array($current->manga_slug, $chapter->chapter_slug))); ?>"><?php echo e(Lang::get('messages.front.reader.chaptre').' '.$chapter->chapter_number.': '. $chapter->chapter_name); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--/ Website Menu -->

        <div class="container-fluid">
            <?php if(!is_null($page)): ?>
            <div class="pager-cnt">
                <div class="row">
                    <div class="col-xs-12">
                        <?php if(isset($prevChapter)): ?>
                        <ul class="pager pull-left" style="margin: 6px 0;">
                            <li class="previous">
                                <a href="#" onclick="return prevChap();"><?php echo e(Lang::get('messages.front.reader.prevChap')); ?></a>
                            </li>
                        </ul>
                        <?php endif; ?>
                        <?php if(isset($nextChapter)): ?>
                        <ul class="pager pull-right" style="margin: 6px 0;">
                            <li class="next">
                                <a href="#" onclick="return nextChap();"><?php echo e(Lang::get('messages.front.reader.nextChap')); ?></a>
                            </li>
                        </ul>
                        <?php endif; ?>
                        <div class="page-nav" style="margin: 0 auto;<?php if ($settings['reader.type'] != 'ppp') { ?> display: none; <?php } ?>">
                            <?php if(config('settings.orientation') === 'rtl'): ?>
                            <ul class="pager">
                                <li class="next">
                                    <a href="#" onclick="return nextPage();"><?php echo e(Lang::get('messages.front.reader.next')); ?></a>
                                </li>
                            </ul>
                            <?php else: ?>
                            <ul class="pager">
                                <li class="previous">
                                    <a href="#" onclick="return prevPage();"><?php echo e(Lang::get('messages.front.reader.prev')); ?></a>
                                </li>
                            </ul>
                            <?php endif; ?>
                            <select id="page-list" class="selectpicker" data-style="btn-primary" data-width="auto" data-size="20">
                                <?php $__currentLoopData = $allPages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pageList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($pageList->page_slug); ?>" <?php if($pageList->page_slug == $page->page_slug): ?> selected <?php endif; ?>><?php echo e($pageList->page_slug); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if(config('settings.orientation') === 'rtl'): ?>
                            <ul class="pager">
                                <li class="previous">
                                    <a href="#" onclick="return prevPage();"><?php echo e(Lang::get('messages.front.reader.prev')); ?></a>
                                </li>
                            </ul>
                            <?php else: ?>
                            <ul class="pager">
                                <li class="next">
                                    <a href="#" onclick="return nextPage();"><?php echo e(Lang::get('messages.front.reader.next')); ?></a>
                                </li>
                            </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class='ads'>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ads-large" style="display: table; margin: 20px auto 10px;">
                            <?php echo isset($ads['TOP_LARGE'])?$ads['TOP_LARGE']:''; ?>

                        </div>
                        <div style="display: table; margin: 20px auto 10px;">
                            <div class="pull-left ads-sqre1" style="margin-right: 50px;">
                                <?php echo isset($ads['TOP_SQRE_1'])?$ads['TOP_SQRE_1']:''; ?>

                            </div>
                            <div class="pull-right ads-sqre2">
                                <?php echo isset($ads['TOP_SQRE_2'])?$ads['TOP_SQRE_2']:''; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="viewer-cnt">
                <div class="row">
                    <div class="col-sm-2 hidden-xs" style="position: relative">
                        <div style="left: 0; right: 0; margin: 0 auto; position: absolute; display: table;">
                            <?php echo isset($ads['LEFT_WIDE_1'])?$ads['LEFT_WIDE_1']:''; ?>

                            <br/>
                            <?php echo isset($ads['LEFT_WIDE_2'])?$ads['LEFT_WIDE_2']:''; ?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <div id="all" style="<?php if ($settings['reader.type'] != 'all') { ?> display: none; <?php } ?>">
                            <?php $__currentLoopData = $allPages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $onePage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <img class="img-responsive" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src='<?php if($onePage->external == 0): ?> <?php echo e(HelperController::pageImageUrl($current->manga_slug,$current->chapter_slug,$onePage->page_image)); ?> <?php else: ?> <?php echo e($onePage->page_image); ?> <?php endif; ?>' alt='<?php echo e($current->manga_name); ?>: Chapter <?php echo e($current->chapter_slug); ?> - Page <?php echo e($onePage->page_slug); ?>'/>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div id="ppp" style="<?php if ($settings['reader.type'] != 'ppp') { ?> display: none; <?php } ?>">
                            <!-- refresh test -->
                            <?php if($settings['reader.mode'] == 'noreload'): ?>
                            <a href="" onclick="return nextPage();">
                                <img class="img-responsive scan-page" src='<?php if($page->external == 0): ?> <?php echo e(HelperController::pageImageUrl($current->manga_slug,$current->chapter_slug,$page->page_image)); ?> <?php else: ?> <?php echo e($page->page_image); ?> <?php endif; ?>' alt='<?php echo e($current->manga_name); ?>: Chapter <?php echo e($current->chapter_slug); ?> - Page <?php echo e($page->page_slug); ?>'/>
                            </a>
                            <?php else: ?>
                            <?php if($page->page_slug < count($allPages) || !is_null($nextChapter)): ?>
                            <a href="<?php if(!is_null($nextChapter) && $page->page_slug == count($allPages)): ?><?php echo e(route('front.manga.reader', array($current->manga_slug,$nextChapter->chapter_slug))); ?><?php else: ?><?php echo e(route('front.manga.reader', array($current->manga_slug,$current->chapter_slug,($page->page_slug)+1))); ?><?php endif; ?>">
                                <img class="img-responsive scan-page" src='<?php if($page->external == 0): ?> <?php echo e(HelperController::pageImageUrl($current->manga_slug,$current->chapter_slug,$page->page_image)); ?> <?php else: ?> <?php echo e($page->page_image); ?> <?php endif; ?>' alt='<?php echo e($current->manga_name); ?>: Chapter <?php echo e($current->chapter_slug); ?> - Page <?php echo e($page->page_slug); ?>'/>
                            </a>
                            <?php else: ?>
                            <img class="img-responsive scan-page" src='<?php if($page->external == 0): ?> <?php echo e(HelperController::pageImageUrl($current->manga_slug,$current->chapter_slug,$page->page_image)); ?> <?php else: ?> <?php echo e($page->page_image); ?> <?php endif; ?>' alt='<?php echo e($current->manga_name); ?>: Chapter <?php echo e($current->chapter_slug); ?> - Page <?php echo e($page->page_slug); ?>'/>
                            <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-2 hidden-xs" style="position: relative">
                        <div style="left: 0; right: 0; margin: 0 auto; position: absolute; display: table;">
                            <?php echo isset($ads['RIGHT_WIDE_1'])?$ads['RIGHT_WIDE_1']:''; ?>

                            <br/>
                            <?php echo isset($ads['RIGHT_WIDE_2'])?$ads['RIGHT_WIDE_2']:''; ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class='ads'>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ads-large" style="display: table; margin: 20px auto 10px;">
                            <?php echo isset($ads['BOTTOM_LARGE'])?$ads['BOTTOM_LARGE']:''; ?>

                        </div>
                        <div style="display: table; margin: 20px auto 10px;">
                            <div class="pull-left ads-sqre1" style="margin-right: 50px;">
                                <?php echo isset($ads['BOTTOM_SQRE_1'])?$ads['BOTTOM_SQRE_1']:''; ?>

                            </div>
                            <div class="pull-right ads-sqre2">
                                <?php echo isset($ads['BOTTOM_SQRE_2'])?$ads['BOTTOM_SQRE_2']:''; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pager-cnt">
                <div class="row">
                    <div class="col-xs-12">
                        <?php if(isset($prevChapter)): ?>
                        <ul class="pager pull-left" style="margin: 6px 0;">
                            <li class="previous">
                                <a href="#" onclick="return prevChap();"><?php echo e(Lang::get('messages.front.reader.prevChap')); ?></a>
                            </li>
                        </ul>
                        <?php endif; ?>
                        <?php if(isset($nextChapter)): ?>
                        <ul class="pager pull-right" style="margin: 6px 0;">
                            <li class="next">
                                <a href="#" onclick="return nextChap();"><?php echo e(Lang::get('messages.front.reader.nextChap')); ?></a>
                            </li>
                        </ul>
                        <?php endif; ?>
                    </div>
                </div>
                        
                <div class="row" style="<?php if ($settings['reader.type'] != 'ppp') { ?> display: none; <?php } ?>">
                    <div class="col-xs-12">
                        <div class="alert alert-warning tips-rtl">
                            <div class="page-header" style="margin: 0">
                                <h1><b><?php echo e($current->manga_name); ?> <?php echo e(' #'.$current->chapter_number.': '. $current->chapter_name); ?></b></h1><?php if(!is_null($page)): ?><span class="pager-cnt" style="<?php if ($settings['reader.type'] != 'ppp') { ?> display: none; <?php } ?>"><small> - <?php echo e(Lang::get('messages.front.reader.page')); ?> <span class="pagenumber"><?php echo e($page->page_slug); ?></span></small></span><?php endif; ?>
                            </div>
                            <div>
                                <strong><?php echo e(Lang::get('messages.front.reader.tips')); ?></strong> 
                                <p>
                                    <?php echo Lang::get('messages.front.reader.tips-message', array('manga' => $current->manga_name, 'chapter' => $current->chapter_number)); ?>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if (is_module_enabled('MySpace')): ?>
            <!-- comment -->
            <?php if(isset($comment->page->reader) && $comment->page->reader == '1'): ?>
            <div class="row" style="max-width: 890px; margin:15px auto;">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php if(isset($comment->builtin) && $comment->builtin == '1'): ?>
                        <li role="presentation" class="active"><a href="#builtin" aria-controls="builtin" role="tab" data-toggle="tab"><?php echo e(Lang::get('messages.front.home.comment.builtin-tab')); ?></a></li>
                        <?php endif; ?>
                        <?php if(isset($comment->fb) && $comment->fb == '1'): ?>
                        <li role="presentation"><a href="#fb" aria-controls="fb" role="tab" data-toggle="tab">Facebook</a></li>
                        <?php endif; ?>
                        <?php if(isset($comment->disqus) && $comment->disqus == '1'): ?>
                        <li role="presentation"><a href="#disqus" aria-controls="disqus" role="tab" data-toggle="tab">Disqus</a></li>
                        <?php endif; ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php if(isset($comment->builtin) && $comment->builtin == '1'): ?>
                        <div role="tabpanel" class="tab-pane active" id="builtin">
                            <input type="hidden" id="post_id" name="post_id" value="<?php echo e($current->chapter_id); ?>"/>
                            <input type="hidden" id="post_type" name="post_type" value="chapter"/>

                            <?php echo $__env->make('front.themes.default.blocs.comments', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($comment->fb) && $comment->fb == '1'): ?>
                        <div role="tabpanel" class="tab-pane" id="fb">
                            <div class="fb-comments" data-width="100%" data-numposts="5" data-colorscheme="dark"
                                 data-href="<?php echo e(route('front.manga.reader', array($current->manga_slug,$current->chapter_slug))); ?>">
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($comment->disqus) && $comment->disqus == '1'): ?>
                        <div role="tabpanel" class="tab-pane <?php if (!isset($comment->fb)) echo 'active'; ?>" id="disqus">
                            <div id="disqus_thread"></div>
                            <script>
                                var disqus_config = function () {
                                    this.page.url = "<?php echo e(route('front.manga.reader', array($current->manga_slug,$current->chapter_slug))); ?>";
                                };
                                (function () {  // DON'T EDIT BELOW THIS LINE
                                    var d = document, s = d.createElement('script');
                                    s.src = '//<?php echo isset($comment->disqusUrl) ? $comment->disqusUrl : '' ?>/embed.js';
                                    s.setAttribute('data-timestamp', +new Date());
                                    (d.head || d.body).appendChild(s);
                                })();</script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php endif; ?>

            <script>
                var title = document.title;
                var pages = <?php echo $allPages; ?>;

                var next_chapter = <?php if(isset($nextChapter)): ?> "<?php echo e(route('front.manga.reader', array($current->manga_slug,$nextChapter->chapter_slug))); ?>" <?php else: ?> "" <?php endif; ?>;
                var prev_chapter = <?php if(isset($prevChapter)): ?> "<?php echo e(route('front.manga.reader', array($current->manga_slug,$prevChapter->chapter_slug, $prevChapterLastPage))); ?>" <?php else: ?> "" <?php endif; ?>;

                var preload_next = 3;
                var preload_back = 2;
                var current_page = <?php echo e($page->page_slug); ?>;

                var base_url = "<?php echo e(route('front.manga.reader', array($current->manga_slug,$current->chapter_slug))); ?>";

                var initialized = false;
                
                jQuery(document).ready(function () {
                    $('.selectpicker').selectpicker();
                    if ($("div#all").is(":visible"))
                        $("div#all img").unveil(300);

                    // refresh test
                    <?php if($settings['reader.mode'] == 'noreload'): ?>
                    jQuery(window).bind('statechange', function () {
                        url = location.href.substr(base_url.length);
                        var State = History.getState();
                        if (url == "") {
                            current_page = 1;
                        } else {
                            url = parseInt(State.url.substr(State.url.lastIndexOf('/') + 1));
                        }
                        changePage(url, false, true);
                        document.title = title;
                    });

                    State = History.getState();
                    url = location.href.substr(base_url.length);
                    if (url == "") {
                        url = 1;
                    } else {
                        url = parseInt(State.url.substr(State.url.lastIndexOf('/') + 1));
                    }

                    current_page = url;
                    History.pushState(null, null, base_url + '/' + current_page);
                    changePage(current_page, false, true);
                    document.title = title;
                    update_numberPanel();
                    <?php else: ?>
                        preload(current_page);
                    <?php endif; ?>
                });

                // refresh test
                <?php if($settings['reader.mode'] == 'noreload'): ?>
                function changePage(id, noscroll, nohash)
                {
                    id = parseInt(id);
                    if (initialized && id == current_page)
                        return false;
                    initialized = true;
                    if (id == pages.length) {
                        if (next_chapter == "") {
                            $('.next').hide();
                        }
                    } else if (id > pages.length) {
                        if (next_chapter == "") {
                            alert('<?php echo e(Lang::get("messages.front.reader.last-page-message")); ?>');
                        } else {
                            location.href = next_chapter;
                        }

                        return false;
                    } else {
                        $('.next').show();
                    }

                    if (id == 1) {
                        if (prev_chapter == "") {
                            $('.previous').hide();
                        }
                    } else if (id <= 0) {
                        if (prev_chapter == "") {
                            alert('<?php echo e(Lang::get("messages.front.reader.first-page-message")); ?>');
                        } else {
                            location.href = prev_chapter;
                        }

                        return false;
                    } else {
                        $('.previous').show();
                    }

                    preload(id);
                    current_page = id;
                    next = parseInt(id + 1);
                    jQuery("html, body").stop(true, true);
                    if (!noscroll)
                        $("html, body").animate({scrollTop: $('div.pager-cnt').eq(0).offset().top});
                    if (pages[current_page - 1].external == 0) {
                        jQuery('.scan-page').attr('src', '<?php echo e(HelperController::pageImageUrl($current->manga_slug,$current->chapter_slug,null)); ?>' + pages[current_page - 1].page_image);
                    } else {
                        jQuery('.scan-page').attr('src', pages[current_page - 1].page_image);
                    }
                    jQuery('.scan-page').parent().attr('href', "<?php echo e(route('front.manga.reader', array($current->manga_slug,$current->chapter_slug))); ?>" + '/' + (current_page + 1));
                    jQuery('.scan-page').attr('alt', '<?php echo e($current->manga_name); ?>: Chapter <?php echo e($current->chapter_slug); ?> - Page ' + current_page);
                    if (!nohash)
                        History.pushState(null, null, base_url + '/' + current_page);
                    document.title = title;
                    update_numberPanel();
                    return false;
                }
                <?php else: ?>
                function changePage(value) {
                    tab= Array();
                    tab[0]="<?php echo e(route('front.manga.reader', array($current->manga_slug,$current->chapter_slug,''))); ?>";
                    tab[1]="/";
                    tab[2]=value;
                    location.href = tab.join("");
                }
                <?php endif; ?>
                
                function nextPage() {
                    // refresh test
                    <?php if($settings['reader.mode'] == 'noreload'): ?>
                    changePage(current_page + 1);
                    return false;
                    <?php else: ?>
                    nextPageVal = $('#page-list option:selected').next().val();
                    nextChapterVal = $('#chapter-list li.active').prev().val();

                    if (typeof nextPageVal != 'undefined') {
                        changePage(nextPageVal);
                    }
                    else if (typeof nextPageVal == 'undefined'
                            && typeof nextChapterVal != 'undefined') {
                        <?php if(!is_null($nextChapter)): ?>
                        location.href = "<?php echo e(route('front.manga.reader', array($current->manga_slug,$nextChapter->chapter_slug))); ?>";
                        <?php endif; ?>
                    } else {
                        alert('<?php echo e(Lang::get("messages.front.reader.last-page-message")); ?>');
                    }
                    <?php endif; ?>
                }

                function prevPage() {
                    // refresh test
                    <?php if($settings['reader.mode'] == 'noreload'): ?>
                    changePage(current_page - 1);
                    return false;
                    <?php else: ?>
                    prevPageVal = $('#page-list option:selected').prev().val();
                    prevChapterVal = $('#chapter-list li.active').next().val();

                    if (typeof prevPageVal != 'undefined') {
                        changePage(prevPageVal);
                    }
                    else if (typeof prevPageVal == 'undefined'
                            && typeof prevChapterVal != 'undefined') {
                        <?php if(!is_null($prevChapter)): ?>
                        location.href = "<?php echo e(route('front.manga.reader', array($current->manga_slug,$prevChapter->chapter_slug,$prevChapterLastPage))); ?>";
                        <?php endif; ?>
                    } else {
                        alert('<?php echo e(Lang::get("messages.front.reader.first-page-message")); ?>');
                    }
                    <?php endif; ?>
                }

                function nextChap(){
                    window.location = next_chapter;
                }
                
                function prevChap(){
                    window.location = <?php if(isset($prevChapter)): ?> "<?php echo e(route('front.manga.reader', array($current->manga_slug,$prevChapter->chapter_slug))); ?>" <?php else: ?> "" <?php endif; ?>;
                }
                
                function preload(id) {
                    var array = [];
                    var arraydata = [];
                    for (i = -preload_back; i < preload_next; i++) {
                        if (id + i >= 0 && id + i < pages.length) {
                            if (pages[(id + i)].external == 0) {
                                array.push('<?php echo e(HelperController::pageImageUrl($current->manga_slug,$current->chapter_slug,null)); ?>' + pages[(id + i)].page_image);
                            } else {
                                array.push(pages[(id + i)].page_image);
                            }
                            arraydata.push(id + i);
                        }
                    }

                    jQuery.preload(array, {
                        threshold: 40,
                        enforceCache: true,
                        onComplete: function (data) {
                        }
                    });
                }

                function update_numberPanel() {
                    $('#page-list').selectpicker('val', current_page);
                    $('.pagenumber').text(current_page);
                }

                $('a#modePPP').click(function (e) {
                    e.preventDefault();
                    $('.pager-cnt .page-nav').show();
                    $('div#ppp').show();
                    $('div#all').hide();
                    $(document).on('keyup', function (e) {
                        KeyCheck(e);
                    });
                });

                $('a#modeALL').click(function (e) {
                    e.preventDefault();
                    $('.pager-cnt .page-nav').hide();
                    $('div#ppp').hide();
                    $('div#all').show();
                    $(document).off('keyup');
                    $("div#all img").unveil(300);
                });

                $('select#page-list').on('change', function () {
                    changePage(this.value);
                });

                $(document).on('keyup', function (e) {
                    KeyCheck(e);
                });

                function KeyCheck(e) {
                    var ev = e || window.event;
                    ev.preventDefault();
                    var KeyID = ev.keyCode;
                    switch (KeyID) {
                        case 36:
                            window.location = "<?php echo e(route('front.manga.show', array($current->manga_slug))); ?>";
                            break;
                        case 33:
                        case 37:
                            prevPage();
                            break;
                        case 34:
                        case 39:
                            nextPage();
                            break;
                    }
                }
                
                <?php if(is_module_enabled('MySpace')): ?>
                    $('a.bookmark').click(function (e) {
                        e.preventDefault();
                        $.ajax({
                            url: "<?php echo e(route('bookmark.store')); ?>",
                            method: 'POST',
                            data: {
                                'manga_id': '<?php echo e($current->manga_id); ?>',
                                'chapter_id': '<?php echo e($current->chapter_id); ?>',
                                'page_slug': '<?php echo e($page->page_slug); ?>',
                                '_token': '<?php echo e(csrf_token()); ?>'
                            },
                            success: function (response) {
                                if (response.status == 'ok') {
                                    alert("<?php echo e(Lang::get('messages.front.bookmarks.bookmarked')); ?>");
                                }
                            },
                            error: function (response) {
                                alert("<?php echo e(Lang::get('messages.front.bookmarks.error')); ?>");
                            }
                        });
                    });
                <?php endif; ?>
            </script>
            <?php else: ?>
            <div style="margin: 100px auto; text-align: center; max-width: 220px;">
                <div class="alert alert-info">
                    <p><?php echo e(Lang::get('messages.front.reader.no-page')); ?></p>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999">
                <div class="modal-content">
                    <?php echo e(Form::open(array('route' => 'front.manga.reportBug', 'role' => 'form'))); ?>

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo e(Lang::get('messages.front.reader.report-broken-image')); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <input type="hidden" name="broken-image" value="<?php echo e($current->manga_name); ?> #<?php echo e($current->chapter_number. ' '); ?><?php if(!is_null($page)): ?><?php echo e(Lang::get('messages.front.reader.page')); ?> <?php echo e($page->page_slug); ?><?php endif; ?>">
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12">
                                <label for="email"><?php echo e(Lang::get('messages.front.reader.email')); ?></label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 controls">
                                <label for="message"><?php echo e(Lang::get('messages.front.reader.message')); ?></label>
                                <textarea rows="3" class="form-control" id="subject" name="subject"></textarea>
                            </div>
                        </div>
                        <br/>
                        <?php if(isset($captcha->form_report) && $captcha->form_report === '1'): ?>
                        <div class="form-group has-feedback <?php echo e($errors->has('g-recaptcha-response') ? ' has-error' : ''); ?>">
                            <?php echo NoCaptcha::renderJs(); ?>

                            <?php echo NoCaptcha::display(); ?>

                            <?php echo $errors->first('g-recaptcha-response', '<span class="help-block">:message</span>'); ?>

                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(Lang::get('messages.front.reader.close')); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo e(Lang::get('messages.front.reader.send')); ?></button>
                    </div>
                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
        <?php if(Session::has('sentSuccess')): ?>
        <div class="modal fade modal-success" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog" style="z-index: 9999">
                <div class="modal-content">
                    <div class="alert text-center alert-info">
                        <?php echo e(Lang::get('messages.front.reader.sentSuccess')); ?>

                    </div>
                </div>
            </div>
        </div>
        <script>$('.modal-success').modal('show')</script>
        <?php elseif($errors->has('g-recaptcha-response')): ?>
        <script>$('#myModal').modal('show')</script>
        <?php endif; ?>
    </body>
</html>
