

<?php $__env->startSection('head'); ?>
<script>
    checked = Array();
    $(document).ready(function () {
        $('.box-body').on('click', 'input[type="checkbox"]', function () {
            if ($(this).prop('checked') == true) {
                if ($(this).val() == 'all') {
                    checked = Array();
                    $('table input[type="checkbox"]').each(function () {
                        $(this).prop('checked', 'checked');
                        checked.push($(this).val());
                    });
                } else {
                    checked.push($(this).val());
                    allChecked = true;
                    $('table input[type="checkbox"]').each(function () {
                        if ($(this).prop('checked') != true) {
                            allChecked = false;
                        }
                    });

                    $('.box-body input.all').prop('checked', allChecked);
                }
            } else {
                if ($(this).val() == 'all') {
                    checked = Array();
                    $('table input[type="checkbox"]').each(function () {
                        $(this).prop('checked', '');
                    });
                } else if (checked.indexOf($(this).val()) != -1) {
                    checked.splice(checked.indexOf($(this).val()), 1);
                    $('.box-body input.all').prop('checked', '');
                }
            }

            $("#pages-ids").val(checked.join(','));
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>
<?php echo Breadcrumbs::render('admin.manga.chapter.show', $manga, $chapter); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-4" >
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-book fa-fw"></i> <?php echo e(Lang::get('messages.admin.chapter.edit.title')); ?>

            </div>
            <!-- /.panel-heading -->
            <?php echo e(Form::open(array('route' => array('admin.manga.chapter.update', $manga->id, $chapter->id), 'method' => 'PUT'))); ?>

            <div class="box-body">
                <?php echo e(Form::open(array('route' => array('admin.manga.chapter.update', $manga->id, $chapter->id), 'method' => 'PUT'))); ?>

                <div class="form-group">
                    <?php echo e(Form::label('name', Lang::get('messages.admin.chapter.create.chapter-name'))); ?>

                    <?php echo e(Form::text('name', $chapter->name, array('class' => 'form-control'))); ?>

                    <?php echo $errors->first('name', '<label class="error" for="name">:message</label>'); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('number', Lang::get('messages.admin.chapter.create.number'))); ?>

                    <?php echo e(Form::text('number', $chapter->number, array('class' => 'form-control'))); ?>

                    <?php echo $errors->first('number', '<label class="error" for="number">:message</label>'); ?>

                </div>

                <div class="form-group">
                    <?php echo e(Form::label('slug', Lang::get('messages.admin.chapter.create.slug'))); ?>

                    <?php echo e(Form::text('slug', $chapter->slug, array('class' => 'form-control', 'placeholder' => Lang::get('messages.admin.chapter.create.slug-placeholder')))); ?>

                    <?php echo $errors->first('slug', '<label class="error" for="slug">:message</label>'); ?>

                </div>              

                <div class="form-group">
                    <?php echo e(Form::label('volume', Lang::get('messages.admin.chapter.create.volume'))); ?>

                    <?php echo e(Form::text('volume', $chapter->volume, array('class' => 'form-control'))); ?>

                </div>
            </div>
            <!-- /.box-body -->
            <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
            || Sentinel::hasAccess('manga.chapter.edit')): ?>
            <div class="box-footer">
                <?php echo e(Form::submit(Lang::get('messages.admin.chapter.edit.update-chapter'), array('class' => 'btn btn-primary btn-xs pull-right'))); ?>

            </div>
            <?php endif; ?>
            <?php echo e(Form::close()); ?>

        </div>
    </div>

    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-book fa-fw"></i> <?php echo e(Lang::get('messages.admin.chapter.edit.chapter-info', array('number' => $chapter->number, 'name' => $chapter->name))); ?>

                <div class="box-tools">
                    <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
                    || Sentinel::hasAccess('manga.chapter.edit')): ?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            <?php echo e(Lang::get('messages.admin.chapter.edit.add-pages')); ?>

                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#" onclick="$('.zipfile').show();
                                        $('#imagesUrlPanel').hide();
                                        return false">
                                    <?php echo e(Lang::get('messages.admin.chapter.edit.upload-zip')); ?>

                                </a>
                            </li>
                            <li>
                                <?php echo e(link_to_route('admin.manga.chapter.page.create', Lang::get('messages.admin.chapter.edit.upload-images'), array('manga' => $manga->id, 'chapter' => $chapter->id))); ?>

                            </li>
                            <li>
                                <a href="#" onclick="$('#imagesUrlPanel').show();
                                        $('.zipfile').hide();
                                        return false">
                                    Add by Images URL
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
                    || Sentinel::hasAccess('manga.chapter.destroy')): ?>
                    <div style="display: inline-block">
                        <?php echo e(Form::open(array('route' => array('admin.manga.chapter.destroy', $manga->id, $chapter->id), 'method' => 'delete'))); ?>

                        <?php echo e(Form::submit(Lang::get('messages.admin.chapter.edit.delete-chapter'), array('class' => 'btn btn-danger btn-xs',  'onclick' => 'if (!confirm("'. Lang::get('messages.admin.chapter.edit.confirm-delete'). '")) {return false;}'))); ?>

                        <?php echo e(Form::close()); ?>

                    </div>
                    <?php endif; ?>

                    <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
                    || Sentinel::hasAccess('manga.chapter.edit')): ?>
                    <div style="display: inline-block;">
                        <?php echo e(Form::open(array('route' => array('admin.manga.chapter.page.destroyPages', $manga->id, $chapter->id), 'method' => 'delete'))); ?>

                        <?php echo e(Form::submit(Lang::get('messages.admin.chapter.edit.delete-pages'), array('class' => 'btn btn-danger btn-xs delete',  'onclick' => 'if (!confirm("'. Lang::get('messages.admin.chapter.edit.confirm-delete-pages'). '")) {return false;}'))); ?>

                        <input type="hidden" name="pages-ids" id="pages-ids"/>
                        <?php echo e(Form::close()); ?>

                    </div>
                    <?php endif; ?>

                    <?php echo e(link_to_route('admin.manga.show', Lang::get('messages.admin.chapter.back'), $manga->id, array('class' => 'btn btn-default btn-xs'))); ?>                
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="box-body">
                <?php if(Session::has('uploadSuccess')): ?>
                <div class="alert text-center alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(Session::get('uploadSuccess')); ?>

                </div>
                <?php elseif(Session::has('uploadError')): ?>
                <div class="alert text-center alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(Session::get('uploadError')); ?>

                </div>
                <?php endif; ?>

                <div class="zipfile well well-sm" style="display: none;">
                    <div class="form-group">
                        <?php if($settings['storage.type'] == 'gdrive'): ?>
                        <div class="alert alert-info" role="alert"><?php echo e(Lang::get('messages.admin.chapter.scraper.storage-mode.gdrive')); ?></div>
                        <?php else: ?>
                        <div class="alert alert-info" role="alert"><?php echo e(Lang::get('messages.admin.chapter.scraper.storage-mode.server')); ?></div>
                        <?php endif; ?>
                    </div>

                    <?php echo e(Form::open(array('route' => 'admin.manga.chapter.uploadZIPFile', 'files' => 'true'))); ?>

                    <div class="form-group">
                        <?php echo e(Form::label('zipfile', Lang::get('messages.admin.chapter.edit.zip-error'))); ?>

                        <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
                        || Sentinel::hasAccess('manga.chapter.edit')): ?>
                        <button class="btn btn-success btn-xs pull-right" type="submit">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span><?php echo e(Lang::get('messages.admin.chapter.edit.upload')); ?></span>
                        </button>
                        <?php endif; ?>
                        <?php echo e(Form::file('zipfile')); ?>

                    </div>
                    <?php echo e(Form::hidden('mangaSlug', $manga->slug, array('id' => 'mangaSlug'))); ?>

                    <?php echo e(Form::hidden('chapterId', $chapter->id, array('id' => 'chapterId'))); ?>

                    <?php echo e(Form::close()); ?>


                    <div id="waiting-upload" style="display: none;">
                        <center><img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" /></center>
                    </div>
                </div>

                <div id="imagesUrlPanel" class="well well-sm" style="display: none;">
                    <button type="button" class="close" aria-label="Close" onclick="$('#imagesUrlPanel').hide();
                            "><span aria-hidden="true">&times;</span></button>
                    <div class="form-group">
                        <label for="imagesUrl"><?php echo e(Lang::get('messages.admin.chapter.edit.add-image-urls')); ?></label>
                        <textarea id="imagesUrl" class="form-control" rows="7"></textarea>
                    </div>

                    <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
                    || Sentinel::hasAccess('manga.chapter.edit')): ?>
                    <button id="startCreatingBtn" type="button" class="btn btn-default" onclick="startCreatingPages()">
                        <?php echo e(Lang::get('messages.admin.chapter.edit.create-pages')); ?>

                    </button>
                    <?php endif; ?>

                    <div id="waiting" style="display: none; float: right;">
                        <img src="<?php echo e(asset('images/ajax-loader.gif')); ?>" />
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="check-all" class="all" value="all"/></th>
                                <th><?php echo e(Lang::get('messages.admin.chapter.edit.page-number')); ?></th>
                                <th><?php echo e(Lang::get('messages.admin.chapter.edit.page-scan')); ?></th>
                                <th><?php echo e(Lang::get('messages.admin.chapter.edit.page-filename')); ?></th>
                                <th><?php echo e(Lang::get('messages.admin.chapter.edit.page-slug')); ?></th>
                                <th><?php echo e(Lang::get('messages.admin.chapter.edit.page-action')); ?></th>
                            </tr>
                        </thead>
                        <?php if(count($chapter->pages) > 0): ?>
                        <tbody>
                            <?php $__currentLoopData = $chapter->pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><input type="checkbox" value="<?php echo e($page->id); ?>"/></td>
                                <td>
                                    <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
                                    || Sentinel::hasAccess('manga.chapter.edit')): ?>
                                    <span class="btn btn-primary btn-xs move-page" data-position="up"><i class="fa fa-arrow-up"></i></span>
                                    <span class="index"><?php echo e($key+1); ?></span>
                                    <span class="btn btn-primary btn-xs move-page" data-position="down"><i class="fa fa-arrow-down"></i></span>
                                    <input type="hidden" value="<?php echo e($page->id); ?>"/>
                                    <?php else: ?>
                                    <span class="index"><?php echo e($key+1); ?></span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <img width="160" alt="<?php echo e($page->image); ?>" src='<?php if($page->external == 0): ?> <?php echo e(HelperController::pageImageUrl("$manga->slug","$chapter->slug","$page->image")); ?><?php echo '?' . time() ?>  <?php else: ?>  <?php echo e($page->image); ?> <?php endif; ?>' />
                                </td>
                                <td class="image">
                                    <?php echo e($page->image); ?>

                                </td>
                                <td class="slug">
                                    <?php echo e($page->slug); ?>

                                </td>
                                <td>
                                    <?php if(((Sentinel::check()->id==$chapter->user->id||Sentinel::check()->id==$manga->user->id) && Sentinel::hasAccess('manage_my_chapters')) 
                                    || Sentinel::hasAccess('manga.chapter.edit')): ?>
                                    <?php echo e(Form::open(array('route' => array('admin.manga.chapter.page.destroy', $manga->id, $chapter->id, $page->id), 'method' => 'delete'))); ?>

                                    <?php echo e(Form::submit(Lang::get('messages.admin.chapter.edit.delete-page'), array('class' => 'btn btn-danger btn-xs', 'onclick' => 'if (!confirm("'. Lang::get('messages.admin.chapter.edit.confirm-delete-page'). '")) {return false;}'))); ?>

                                    <?php echo e(Form::close()); ?>

                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                        <?php else: ?>
                        <tr>
                            <td colspan="6">
                                <div class="center-block"><?php echo e(Lang::get('messages.admin.chapter.edit.no-page')); ?></div>
                            </td>
                        </tr>
                        <?php endif; ?>
                    </table>
                </div>                       
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
<script>
    var chapterId = '<?php echo e($chapter->id); ?>';
    var mangaId = '<?php echo e($manga->id); ?>';
    var urls = Array();

    $(document).ready(function () {
        $('.zipfile form').on('submit', function () {
            $('#waiting-upload').show();
        });

        $('table .move-page').first().attr('disabled', 'disabled');
        $('table .move-page').last().attr('disabled', 'disabled');

        $(".move-page").click(function () {
            position = $(this).data('position');
            tr = $(this).parents('tr');

            $.ajax({
                method: "POST",
                url: "<?php echo e(route('admin.manga.chapter.movePage')); ?>",
                data: {'index': tr.index(), 'mangaSlug': "<?php echo e($manga->slug); ?>", 'chapterId': chapterId, 'position': position, '_token': '<?php echo e(csrf_token()); ?>'},
                success: function (response) {
                    tr.find('span.index').text(response.p1.slug);
                    tr.find('td.image').text(response.p1.image);
                    tr.find('td.slug').text(response.p1.slug);

                    if (position == 'up') {
                        tr.prev('tr').find('span.index').text(response.p2.slug);
                        tr.prev('tr').find('td.image').text(response.p2.image);
                        tr.prev('tr').find('td.slug').text(response.p2.slug);

                        tr.prev('tr').before(tr);
                    } else {
                        tr.next('tr').find('span.index').text(response.p2.slug);
                        tr.next('tr').find('td.image').text(response.p2.image);
                        tr.next('tr').find('td.slug').text(response.p2.slug);

                        tr.next('tr').after(tr);
                    }

                    $('table .move-page').removeAttr('disabled');
                    $('table .move-page').first().attr('disabled', 'disabled');
                    $('table .move-page').last().attr('disabled', 'disabled');
                }
            });
        });
    });

    function startCreatingPages() {
        if (!$('#imagesUrl').val().length) {
            alert('Please enter a valid image URL!');
            return;
        }

        urls = $.trim($('#imagesUrl').val()).split('\n');
        var patt = new RegExp(/\.(jpeg|jpg|gif|png|bmp)$/);
        for (i = 0; i < urls.length; i++) {
            if (!patt.test(urls[i])) {
                alert('Some of your URLs are invalid! Only image files are allowed.');
                return;
            }
        }

        if ($('span#errors').length > 0) {
            $('span#errors').remove();
        }

        $('#startCreatingBtn').addClass('disabled');
        $('#waiting').show();

        $.ajax({
            method: "POST",
            url: "<?php echo e(route('admin.manga.chapter.createExternalPages')); ?>",
            data: {'urls': $.trim($('#imagesUrl').val()).replace(/\n/g, ';'), 'mangaId': mangaId, 'chapterId': chapterId, '_token': '<?php echo e(csrf_token()); ?>'},
            success: function (response) {
                $('#waiting').hide();
                window.location = window.location;
            },
            error: function (xhr, status, error) {
                $('#startCreatingBtn').removeClass('disabled');
                $('#waiting').hide();

                var err = xhr.responseJSON;
                if (typeof err !== "undefined") {
                    if ($('span#errors').length > 0) {
                        $('span#errors').remove();
                    }

                    $('#startCreatingBtn').after('<span id="errors"> ' + err.error.type + ' [' + err.error.message + ']</span>');
                }
            }
        });
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>