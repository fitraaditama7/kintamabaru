

<?php $__env->startSection('title'); ?>
<?php if(isset($seo->latestrelease->title->global) && $seo->latestrelease->title->global == '1'): ?>
<?php echo e($settings['seo.title']); ?> | <?php echo e(Lang::get('messages.front.home.latest-manga')); ?>

<?php else: ?>
<?php echo e($seo->latestrelease->title->value); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
<?php if(isset($seo->latestrelease->description->global) && $seo->latestrelease->description->global == '1'): ?>
<?php echo e($settings['seo.description']); ?>

<?php else: ?>
<?php echo e($seo->latestrelease->description->value); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('keywords'); ?>
<?php if(isset($seo->latestrelease->keywords->global) && $seo->latestrelease->keywords->global == '1'): ?>
<?php echo e($settings['seo.keywords']); ?>

<?php else: ?>
<?php echo e($seo->latestrelease->keywords->value); ?>

<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.themes.'.$theme.'.blocs.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startSection('allpage'); ?>
<h2 class="listmanga-header">
    <i class="fa fa-calendar-o"></i><?php echo e(Lang::get('messages.front.home.latest-manga')); ?>

</h2>
<hr/>

<?php if(count($latestMangaUpdates)>0): ?>
<div class="mangalist">
    <?php $__currentLoopData = $latestMangaUpdates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $date => $dateGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php $__currentLoopData = $dateGroup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $manga): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="manga-item">
        <h3 class="manga-heading <?php if(config('settings.orientation') === 'rtl'): ?> pull-right <?php else: ?> pull-left <?php endif; ?>">
            <i class="fa fa-book"></i>
            <a href="<?php echo e(route('front.manga.show',$manga['manga_slug'])); ?>"><?php echo e($manga["manga_name"]); ?></a>
            <?php if($manga["hot"]): ?>
            <span class="label label-danger"><?php echo e(Lang::get('messages.front.home.hot')); ?></span>
            <?php endif; ?>
        </h3>
        <small class="<?php if(config('settings.orientation') === 'rtl'): ?> pull-left <?php else: ?> pull-right <?php endif; ?>" style="direction: ltr;">  
            <?php if($date == 'Y'): ?>
            <?php echo e(Lang::get('messages.front.home.yesterday')); ?>

            <?php elseif($date == 'T'): ?>
            <?php echo e(Lang::get('messages.front.home.today')); ?>

            <?php else: ?>
            <?php echo e($date); ?>

            <?php endif; ?>
        </small>
        <?php $__currentLoopData = $manga['chapters']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chapter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="manga-chapter">
            <h6 class="events-subtitle">
                <?php echo e(link_to_route('front.manga.reader', "#".$chapter['chapter_number'].". ".$chapter['chapter_name'], [$manga['manga_slug'], $chapter['chapter_slug']])); ?>

            </h6>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        
</div>

<div class="row">
    <div class="col-xs-12">
        <?php echo e($latestMangaUpdates->links()); ?>

    </div>
</div>
<?php else: ?>
<div class="center-block">
    <p><?php echo e(Lang::get('messages.front.home.no-chapter')); ?></p>
</div>
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>