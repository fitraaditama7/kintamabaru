<?php $__env->startSection('title'); ?>
Complete reset password
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="login-box">
    <div class="login-logo">
        <a href="<?php echo e(route('front.index')); ?>"><b><?php echo e($sitename); ?></b></a>
    </div>
    <?php echo $__env->make('base::admin._partials.notifications', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="login-box-body">
        <p class="login-box-msg"><?php echo e(trans('user::messages.front.auth.reset_password')); ?></p>

        <?php echo e(Form::open()); ?>

        <div class="form-group has-feedback <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
            <input type="password" class="form-control" autofocus
                   name="password" placeholder="<?php echo e(trans('user::messages.front.auth.password')); ?>">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?php echo $errors->first('password', '<span class="help-block">:message</span>'); ?>

        </div>
        <div class="form-group has-feedback <?php echo e($errors->has('password_confirmation') ? ' has-error has-feedback' : ''); ?>">
            <input type="password" name="password_confirmation" class="form-control" placeholder="<?php echo e(trans('user::messages.front.auth.password_confirmation')); ?>">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            <?php echo $errors->first('password_confirmation', '<span class="help-block">:message</span>'); ?>

        </div>

        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat pull-right">
                    <?php echo e(trans('user::messages.front.auth.reset_password')); ?>

                </button>
            </div>
        </div>
        <?php echo e(Form::close()); ?>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user::layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>