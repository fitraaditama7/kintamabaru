<?php
$settings = Cache::get('options');
$theme = Cache::get('theme');
$variation = Cache::get('variation');
?>


<?php $__env->startSection('title'); ?>
<?php echo e($settings['seo.title']); ?> | 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
<style>
    .error-page {
        width: 600px;
        margin: 100px auto;
    }
    .error-page>.headline {
        float: left;
        font-size: 100px;
        font-weight: 300;
    }
    .error-page>.error-content {
        margin-left: 190px;
        display: block;
    }
    .error-page>.error-content>h3 {
        font-weight: 300;
        font-size: 25px;
    }
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.themes.'.$theme.'.blocs.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startSection('allpage'); ?>
<div class="error-page">
    <h2 class="headline text-yellow" style="line-height: 0.6; margin-top: 0;"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> <?php echo e(trans('messages.error_404_title')); ?></h3>
        <p><?php echo e(trans('messages.error_404_description')); ?></p>
    </div>
    <!-- /.error-content -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.'.$theme, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>