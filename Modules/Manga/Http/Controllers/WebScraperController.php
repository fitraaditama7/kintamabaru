<?php

namespace Modules\Manga\Http\Controllers;

use Goutte\Client;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;
use Modules\Base\Entities\Option;
use Modules\Manga\Entities\Chapter;
use Modules\Manga\Entities\Manga;

/**
 * Reader Controller Class
 * 
 * PHP version 5.4
 *
 * @category PHP
 * @package  Controller
 * @author   cyberziko <cyberziko@gmail.com>
 * @license  commercial http://getcyberworks.com/
 * @link     http://getcyberworks.com/
 */
class WebScraperController extends Controller
{

    private $client;
    private $website;
    private $websites;
    public $chapterUrl;
    public $crawler;
    public $filters;
    public $content = array();

    /**
     * Defining our Dependency Injection Here.
     * or Instantiate new Classes here.
     *
     * @param Client $client current client
     * 
     * @return void
     */
    public function __construct(Client $client)
    {
        $client->setHeader('User-Agent', "Google Mozilla/5.0 (compatible; Googlebot/2.1;)");
        $client->setHeader('Referer', "http://www.google.com/bot.html");
    
        $this->client = $client;
		
        $this->websites = [
            'komikcast'    => [
                'name'	=> 'KomikCast (Indonesia)',
                'url'	=> 'https://komikcast.com',
                'filter'=> [
                    'page_list'     => '',
                    'image_src'     => '',
                ]
            ],
            'kinggrabber'    => [
                'name'	=> 'King Grabber (Global)',
                'url'	=> 'https://app.kinggrabber.me',
                'filter'=> [
                    'page_list'     => '',
                    'image_src'     => '',
                ]
            ]
        ];
    }

    /**
     * Start scraping
     * 
     * @param type $mangaId   manga id
     * @param type $chapterId chapter id
     * 
     * @return type
     */
    public function scraper($mangaId)
    {
        $manga = Manga::find($mangaId);
        $settings = Cache::get('options');
        
        return view(
            'manga::admin.manga.chapter.scraper', 
            ['manga' => $manga, 'websites' => $this->websites, 'settings' => $settings]
        );
    }

    /**
     * This will be used for Outputing our Data
     * and Rendering to browser.
     *
     * @return void
     */
    public function startScraper() 
    {
        $selectedWebsite = filter_input(INPUT_POST, 'selectedWebsite');
        $mangaId = filter_input(INPUT_POST, 'mangaId');
        $host_id = filter_input(INPUT_POST, 'host_id');
        $chapterUrl = filter_input(INPUT_POST, 'chapterUrl');
        
        $this->website = $this->websites[$selectedWebsite]['url'];
        $this->chapterUrl = $chapterUrl;
        $this->setScrapeUrl($this->chapterUrl);

        $this->filters = $this->websites[$selectedWebsite]['filter'];

        switch ($selectedWebsite) {
            case 'komikcast':
                return $this->_komikcast($mangaId, $chapterUrl);
            case 'kinggrabber':
                return $this->_kinggrabber($mangaId, $host_id, $chapterUrl);
        }
    }

    /**
     * Setup our scraper data. Which includes the url that
     * we want to scrape
     *
     * @param String $url    = default is NULL 
     * @param String $method = Method Types its either POST || GET
     * 
     * @return void
     */
    public function setScrapeUrl($url = null, $method = 'GET')
    {
        $this->crawler = $this->client->request($method, $url);
    }
    
    public function setScrapeJson($url = null, $method = 'GET')
    {
        
        $this->client->setHeader('X-KEY', '54bc6d4bf9b42e58e3536b10cb760ed0');
        $this->crawler = $this->client->request($method, $url);
    }

    /**
     * This will get all the return Result from our Web Scraper
     *
     * @return array
     */
    public function getContents() 
    {        
        $countContent = $this->crawler
            ->filter($this->filters['page_list'])->count();
        if ($countContent) {
            $this->content = $this->crawler
                ->filter($this->filters['page_list'])
                ->each(
                    function ($node, $i) {
                        return [
                            'index' => $node->text(),
                            'url' => $this->_getImageSrc(
                                $this->website . $node->attr('value')
                            )
                        ];
                    }
                );
        }

        return $this->content;
    }

    /**
     * Get image src
     * 
     * @param type $nextPageUrl url
     * @param type $method      GET
     * 
     * @return type
     */
    private function _getImageSrc($nextPageUrl, $method = 'GET') 
    {
        $this->client->setHeader('timeout', '60');
        $imgCrawler = $this->client->request($method, $nextPageUrl);
        return $imgCrawler->filter($this->filters['image_src'])->attr('src');
    }
    /**
     * http://www.pecintakomik.com
     * @param type $mangaId
     * @param type $chapterUrl
     * @return type
     */
    
    private function _specinta($mangaId, $chapterUrl)
    {
        $chapterNumber = trim();

        $title = 'chapter '. $chapterNumber;
        
        // $chapterId = $this->_createChapter($mangaId, $chapterNumber, $title);
        
        // if($chapterId == 'exist') {
        //     return Response::json(
        //         ['chapterId' => $chapterId, 'chapterNumber' => $chapterNumber]
        //     );
        // }
        $chapterId = 1;
        $countContent = $this->crawler
            ->filter($this->filters['page_list'])->count();
        if ($countContent) {
            $this->content = $this->crawler
                ->filter($this->filters['page_list'])
                ->each(
                    function ($node, $i) {
                        return [
                            'index' => $node->text(),
                            'url' => str_replace( ' ', '%20', 'http://www.pecintakomik.com/'.$this->_getImageSrc(
                                rtrim($this->chapterUrl,'/') . '/'. $node->attr('value'))
                            )
                        ];
                    }
                );
        }
        
        return Response::json(
            [
                'contents' => $this->content,
                'chapterId' => $chapterId,
                'chapterNumber' => $chapterNumber
            ]
        );
    }

    /**
     * http://app.kinggrabber.me
     * @param type $mangaId
     * @param type $chapterUrl
     * @param type $host_id
     * @return type
     */
    
    private function _kinggrabber($mangaId, $host_id, $chapterUrl)
    {
        $json = json_decode($this->crawler->text());
        $chapterNumber = trim($json->chapter);

        $title = 'chapter '. $chapterNumber;
        
        $chapterId = $this->_createChapter($mangaId, $chapterNumber, $title);
        
        if($chapterId == 'exist') {
            return Response::json(
                ['chapterId' => $chapterId, 'chapterNumber' => $chapterNumber]
            );
        }
        $k = 0;
        foreach ($json->data as $value) {
            $data[$k]['index'] =  $k+1;
            $data[$k]['url'] =  $value;
            $k++;
        }
        $this->content = $data;
        return Response::json(
            [
                'contents' => array_values(array_filter($this->content)),
                'chapterId' => $chapterId,
                'chapterNumber' => sprintf('%02d', $chapterNumber)
            ]
        );
    }
    /**
     * http://www.komikcast.com
     * @param type $mangaId
     * @param type $chapterUrl
     * @return type
     */
    
    private function _komikcast($mangaId, $chapterUrl)
    {
        $chapterNumber = $this->getStr($this->crawler->filter('title')->text(),'Chapter','Bahasa');
        $title = 'chapter '. $chapterNumber;
        $chapterId = $this->_createChapter($mangaId, $chapterNumber, $title);
        
        if($chapterId == 'exist') {
            return Response::json(
                ['chapterId' => $chapterId, 'chapterNumber' => $chapterNumber]
            );
        }
        $this->content =  $this->crawler->filter('article .maincontent #readerarea img')
        ->each(
            function ($node, $i) {
                if(strlen($node->attr('src')) > 5){
                    return [
                        'index' => $i,
                        'url' => $node->attr('src')
                    ];
                }
            }
        );
        return Response::json(
            [
                'contents' => array_values(array_filter($this->content)),
                'chapterId' => $chapterId,
                'chapterNumber' => sprintf('%02d', $chapterNumber)
            ]
        );
    }
    private function getStr($string,$start,$end){
		$str = explode($start,$string,2);
		$str = explode($end,$str[1],2);
		return $str[0];
	}
    
    /***/
    private function _createChapter($mangaId, $chapterNumber, $title, $checkChapterExist = true)
    {
    	$chapterExist = 0;
        
        if($checkChapterExist) {
            // allow duplicate?
            $mangaOptions = json_decode(Option::where('key', '=' , 'manga.options')->first()->value);

            if (isset($mangaOptions->allow_duplicate_chapter) 
                && $mangaOptions->allow_duplicate_chapter == '1') {
                $checkChapterExist = false;
            }
        }
        
    	if($checkChapterExist) {
            $chapterExist = Chapter::join("manga", "manga.id", "=", "manga_id")
                ->where(function ($query) use($chapterNumber) {
                    $query->where('number', '=', $chapterNumber)
                    ->orWhere('chapter.slug', '=', $chapterNumber);
                })
                ->where('manga_id', '=', $mangaId)
                ->count();
    	}

        if ($chapterExist > 0) {
            return 'exist';
        } else {
            $chapter = new Chapter();
            
            $chapter->name = $title;
            $chapter->slug = $chapterNumber;
            $chapter->number = $chapterNumber;
            $chapter->user_id = Sentinel::check()->id;
            
            $manga = Manga::find($mangaId);
            $savedChapter = $manga->chapters()->save($chapter);

            return $savedChapter->id;
       }
    }

    // ---------- Bulk Scraping ---------- \\ 
    public function getTotalChapters()
    {
        $mangaPageUrl = filter_input(INPUT_POST, 'mangaPageUrl');
        $this->setScrapeUrl($mangaPageUrl);
        $content = array();

        if(strpos($mangaPageUrl, '3asq.info')) {
            $pages = array();
            
            if($this->crawler->filterXPath('(//ul[@class="pgg"])[1]//a')->count()>1){
                $pages = $this->crawler->filterXPath('(//ul[@class="pgg"])[1]//a')->each(
                    function ($node) {
                        if(is_numeric($node->text())) {
                            return $node->attr('href');
                        }
                    }
                );
                
                foreach ($pages as $page) {
                    if(!is_null($page)) {
                        $pageCrawler = $this->client->request('GET', $page);
                        $table = $pageCrawler->filter('ul.lst a.lst')->each(
                            function ($node) {
                                return ['url' => $node->attr('href')];
                            }
                        );
                        
                        foreach ($table as $value) {
                            array_push($content, $value);
                        }
                    }
                }
            } else {
                $content = $this->crawler->filter('ul.lst a.lst')->each(
                    function ($node) {
                        return ['url' => $node->attr('href')];
                    }
                ); 
            }
            
            $total = count($content);
        }else if(strpos($mangaPageUrl, 'komikcast.com')) {
            $tableListing = $this->crawler->filter('.mangainfo');
            
            $total = $tableListing->filter('.cl ul li span a')->count();
            
            if ($total > 0) {
                $content = $tableListing->filter('.cl ul li span a')
                    ->each(
                        function ($node, $i) {
                            return [
                                'url' => $node->attr('href'),
                                'title' =>  $node->text()
                            ];
                        }
                    );
                $content = array_reverse($content);
            }
        }else {
            $tableListing = $this->crawler->filter('#listing');

            $total = $tableListing->filter('#listing td a')->count();

            if ($total > 0) {
                $content = $tableListing->filter('#listing td a')
                    ->each(
                        function ($node, $i) {
                            return [
                                'url' => $node->attr('href'),
                                'title' =>  trim(substr(strstr($node->parents()->first()->text(), ":"), 1))
                            ];
                        }
                    );
            }
        }
        
        return Response::json(
            [
                'total' => $total,
                'content' => $content
            ]
        );
    }
	
    public function getChapter() 
    {
        $selectedWebsite = filter_input(INPUT_POST, 'selectedWebsite');
        $mangaId = filter_input(INPUT_POST, 'mangaId');
        $host_id = filter_input(INPUT_POST, 'host_id');
        $chapterUrl = filter_input(INPUT_POST, 'chapterUrl');
        $title = filter_input(INPUT_POST, 'chapterTitle');
		
        $this->website = $this->websites[$selectedWebsite]['url'];
	
        if($selectedWebsite == 'kinggrabber') {
            $this->chapterUrl = $chapterUrl;
            $this->setScrapeJson('https://app.kinggrabber.me/api/v1/grab/'.$host_id.'?url='.$this->chapterUrl);
            $cnt =  $this->_kinggrabber($mangaId, $host_id, $this->chapterUrl);
            return $cnt;
        } else if ($selectedWebsite == 'komikcast') {
            $this->chapterUrl = $chapterUrl;
            $this->setScrapeUrl($this->chapterUrl);
    
            $this->filters = $this->websites[$selectedWebsite]['filter'];
            $cnt =  $this->_komikcast($mangaId, $this->chapterUrl);
            return $cnt;
        }
        
        $chapterNumber = substr($chapterUrl, strrpos($chapterUrl, '/')+1);
        $chapterId = $this->_createChapter($mangaId, $chapterNumber, $title, false);
        
        return Response::json(
            [
                'contents' => $this->getContents(),
                'chapterId' => $chapterId,
                'chapterNumber' => $chapterNumber
            ]
        );
    }

    public function abort() 
    {
    	$mangaId = filter_input(INPUT_POST, 'mangaId');
        $bulkStatus = filter_input(INPUT_POST, 'bulkStatus');
		
        $manga = Manga::find($mangaId);
        $manga->bulkStatus =$bulkStatus;
        $manga->save();
    }
	
    public function resume() 
    {
        $mangaId = filter_input(INPUT_POST, 'mangaId');
        $manga = Manga::find($mangaId);

        return Response::json(
            [
                'bulkStatus' => $manga->bulkStatus
            ]
        );
    }
}
